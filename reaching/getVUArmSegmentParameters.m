function p = getVUArmSegmentParameters()
  p.l = [0.3348;0.2628];
  p.d = [0.1460;0.1792];
  p.r = p.l-p.d;
  p.m = [2.1;1.65];
  p.I = [0.0244;0.0250];
  uBlank = [spline([0,10],[0,0]);spline([0,10],[0,0])];
  p.U = uBlank;