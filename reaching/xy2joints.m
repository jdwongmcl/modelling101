function out = xy2joints(xy,lengths)
%function out = xy2joints(xy,lengths)
% converts handspace x and y to joint angles. 
% assumes shoulder at [0,0]!!
% assumes solution is for the right arm (since you have to make a choice
% about putting the arm somewhere)
% output is two external-reference-frame angles. 
x=xy(1);y=xy(2);
l1 = lengths(1);
l2 = lengths(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

out = [q1,q2];