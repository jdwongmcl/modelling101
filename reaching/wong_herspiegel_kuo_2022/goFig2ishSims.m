sweeps = load('optDurForReward.mat')

%% across different costs, plot dur vs dist, vel vs dist
figure();
for jValuation = 1:size(sweeps.trajectories,2)
    subplot(2,2,1);
    xDist = sweeps.dists(:,jValuation);
    yDuration = sweeps.tSol(:,jValuation);
    notNan = ~isnan(yDuration);
    plot(xDist(notNan),yDuration(notNan));hold on;
end
ylim([0,2.5])
%% across different costs, plot dist/dur, vel vs dist
for jValuation = 1:size(sweeps.trajectories,2)
    subplot(2,2,2);
    xDist = sweeps.dists(:,jValuation);
    yMaxSpeed = sweeps.maxspeed(:,jValuation);
    notNan = ~isnan(yMaxSpeed);
    plot(xDist(notNan),yMaxSpeed(notNan));hold on;
end

xlabel('Distance (m)')
ylabel('Peak Speed (m/s)')

%% for the third plot, just plot extremes
valuationLow = 0.25;
valuationHigh = 5;
jLow = find(sweeps.rewards==valuationLow)
jHigh = find(sweeps.rewards == valuationHigh)
subplot(2,2,3);
for i = 1:size(sweeps.trajectories,1)
    curMov = sweeps.trajectories(i,jLow);
    plot(curMov.t_plot,curMov.,'b');
    hold on;
end
for i = 1:size(sweeps.trajectories,1)
    plot(sweeps.trajectories(i,jHigh),'r');
    hold on;
end