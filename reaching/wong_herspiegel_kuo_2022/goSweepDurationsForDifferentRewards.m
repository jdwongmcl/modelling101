rewards = [1,2,3,4,5,.5,.25]
for ir=4:length(rewards)
    dists = [.01,.02,.03,.05,.1:.05:.5]
    for i = 1:length(dists)
    dist =dists(i);
    x0 = [0,.15];
    x1 = [0,0.15+dist];
    reward = rewards(ir);
    [EdotCost(i,ir), EdotCostMp(i,ir), EdotFR(i,ir), EdotReward(i,ir),tSol(i,ir),...
    maxspeed(i,ir),trajectories(i,ir), ExitFlags(i,ir),Informs(i,ir)]=...
    minimizeEnergyAndTimeForPointToPoint(reward, ...
    x0,x1, ...
    'nodes',[6,10,20,30],...
    'dampcoef',0.0)
     dists(i,ir) = dist;
    end
    close all;
end
%%
save('optDurForReward20220318','rewards','EdotCost','EdotCostMp','EdotFR','EdotReward',...
    'maxspeed','trajectories','tSol','ExitFlags','Informs','dists','reward')
