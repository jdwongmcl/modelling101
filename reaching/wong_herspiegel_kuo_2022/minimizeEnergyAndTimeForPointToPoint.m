function [EdotCostTotal, EdotCostMechPower, EdotCostForceRate, EdotReward, tendSol, vPeak, trajectories, ExitFlags,Informs]= minimizeEnergyAndTimeForPointToPoint(cReward, xyBeg,xyEnd,varargin)
%function [Edot, EdotCostForceRate, EdotCostWork, EdotReward, duration, trajectories]= solveForPreferredDistance(cReward, xyBeg,xyEnd)

% defaults
%'namesim' nameSim=['Task-DistanceAndDuration-Min_FoRaTi'];
%'nodes' nodeLevels = [10:10:30];
%'coeffrr' cForceRateRateLin = 8.5e-2; % linear cost to force rate
%'verbose' verbose = 1;
clc;
warning off

%%% processing optional args
% defaults
nameSim = ['Task-DistanceAndDuration-Min_FoRaTi'];
nodeLevels = [10:10:30];
cForceRateRateLin = 8.5e-2; % linear cost to force rate
verbose = 1;% show figures =1
inDampCoef = 0;
handMass = 0;% note this will override the handMass in the parameter file.
for i = 1 : 2 : length(varargin)
    option = varargin{i};
    value = varargin{i + 1};
    switch option
        case 'namesim'
            nameSim = value;
        case 'nodes'
            nodeLevels = value;
        case 'coeffrr'
            cForceRateRateLin = value;
        case 'verbose'
            verbose = value;
        case 'dampcoef'
            inDampCoef = value;
        case 'handMass'
            handMass = value;
            fprintf('overwriting hand mass in parameter file.\n');
    end
end
%%% done processing


%% coefficients for force rate, and UNO.

cForceRateRateQuad = 1.2e-4; % quadratic cost 1.2e-4;%
LIN = 1; QUAD = 2;
LIN1QUAD2 = LIN; % JDW:we can assume either a quadratic or linear fraterate cost. they really do matter,
% and sadly linear is more realistic but also takes longer.
% hence why we have kept quadratic around; if the precision
% isn't necessary, you can quickly run a quadratic cost
cUnoTop = 1e-2; % we should in principle perhaps make this fit about right for our experiment.
%cReward = 10;%this is a linear time cost per second, in W. uno 15. wfr 25
kMechanicalWorkEfficiencyMargaria = 4.2;

%% compute the angles given cartesian targets
P = load('paramsTomlabJDW.mat');
theta_start = xy2joints(xyBeg,[P.l(1),P.l(2)]);
theta_start = theta_start(:);
theta_target = xy2joints(xyEnd,[P.l(1),P.l(2)]);
theta_target = theta_target(:);
if sum(imag([theta_target;theta_start])) ~= 0
    fprintf('cannot reach one of the targets!\n');
    return
end

% now loop over optionally different number of time nodes
fprintf('entering optimization loop of node numbers:\n')
disp(nodeLevels);
for iNode = 1:length(nodeLevels)
    if iNode==1
        tic
    end
    disp(['Round: ',num2str(iNode)])
    
    %%%setup the tomlab problem by defining the
    %%%1 TIME
    %%%2 PHASE
    %%%3 CONTROLS
    %%%4 STATES
    %%%5 SLACK variables (for computing work/fr, this works well.
    toms t tEnd;
    steps = nodeLevels(iNode);
    p = tomPhase('p', t, 0, tEnd, steps, [], 'spline3');
    setPhase(p);
    theta = tomState('theta',2,1);        % segment angles
    thetaDot = tomState('thetadot',2,1);      % segment angular velocities
    torque = tomControl('torque',2,1);
    
    % slack variables
    mechPowerConEq = tomState('mechPowerConEq',2,1);

    frrConEq = tomState('frrConEq',2,1);
    frrConNegEq = tomState('frrConNegEq',2,1); 
    %%%/setup the tomlab problem by defining time, phase, controls,
    %%%states, slack variables
    
    %%% initial guess
    if iNode==1
        %initial guess
        x0 = {
            tEnd == .3; % NOTE!: if duration tEnd IS optimizable, then TOMLAB NEEDS it to BE FIRST in the x0 list. see propt documentation.
            icollocate({
            theta  == vec(interp1([0;tEnd],[theta_start'; theta_target'],t))
            thetaDot  == (theta_target-theta_start)/2;%jdw hack
            })
            icollocate({
            torque  == vec(interp1([0 tEnd],[0,0; 1,1],t));
            })
            };
        %%% otherwise warmstart
    else
        x0 = {
            tEnd == tendopt
            icollocate({
            theta == fiopt
            thetaDot == fidopt
            })
            
            icollocate({
            torque == stimopt
            })
            };
    end
    
    %%% Boundary constraints. Start at the target, with zero speed, and
    %%% zero acceleration.
    cbnd = {
        initial({
        theta == theta_start;
        thetaDot == [0;0];
        dot(thetaDot) == [0;0];
        })
        final({theta == theta_target;
        thetaDot == 0;
        dot(thetaDot)==0;
        })
        };
    
    %%% Box constraints. can help the solver sometimes.
    cbox = {
        mcollocate(-pi/2 <= theta(1) <= 3*pi/2)
        mcollocate(-pi/4 <= theta(2)-theta(1) <= pi)
        mcollocate(-100 <= thetaDot <= 100)% note that this overloads thetaDot since it's a vector and 100 is a scalar, which is okay.
        -100 <=  collocate(torque)  <= 100
        0 <=tEnd <=10
        };
    %%%
    % ODEs and path constraints via virtual power
    % P was set above near line 20 =)
    P = load('paramsKinarmValidated80KgSubj.mat');
    m1 = P.L1_M;
    m2 = P.L2_M;
    m3 = P.L3_M;
    m4 = P.L4_M;
    
    I1 = P.L1_I;
    I2 = P.L2_I;
    I3 = P.L3_I;
    I4 = P.L4_I;
    
    l1 = P.L1_L;
    l3 = P.L3_L;
    cx1 = P.L1_C_AXIAL;
    ca1 = P.L1_C_ANTERIOR;
    cx2 = P.L2_C_AXIAL;
    ca2 = P.L2_C_ANTERIOR;
    cx3 = P.L3_C_AXIAL;
    ca3 = P.L3_C_ANTERIOR;
    cx4 = P.L4_C_AXIAL;
    ca4 = P.L4_C_ANTERIOR;
    Q25 = P.L2_L5_ANGLE;
    
    mHandMass = handMass;
    cHandMass = P.cHandMass;
    IHandMass = 0;
    Imot1 = 0.001;
    Imot2 = 0.001;
    M11 = I1 + I4 + Imot1 + ca1^2*m1 + ca4^2*m4 + cx1^2*m1 + cx4^2*m4 + l1^2*m2 + l1^2*mHandMass;
    M12 = cx2*l1*m2*cos(theta(1) - theta(2)) - ca4*l3*m4*sin(Q25 + theta(1) - theta(2)) + ...
        cHandMass*l1*mHandMass*cos(theta(1) - theta(2)) + ca2*l1*m2*sin(theta(1) - theta(2)) + cx4*l3*m4*cos(Q25 + theta(1) - theta(2));
    %M12 = cx2*l1*m2*cos(theta(1) - theta(2)) - ca4*l3*m4*sin(Q25 + theta(1) - theta(2)) + ca2*l1*m2*sin(theta(1) - theta(2)) + cx4*l3*m4*cos(Q25 + theta(1) - theta(2));
    M21 = M12; %Kane/Lagrange derivations produce a symmetric mass matrix.
    M22 = mHandMass*cHandMass^2 + m2*ca2^2 + m3*ca3^2 + m2*cx2^2 + m3*cx3^2 ...
        + m4*l3^2 + I2 + I3 + IHandMass + Imot2;
    
    
    
    F1 = -thetaDot(2)^2*(cx4*l3*m4*sin(Q25 + theta(1) - theta(2)) - ca2*l1*m2*cos(theta(1) - theta(2)) + ...
        cx2*l1*m2*sin(theta(1) - theta(2)) + cHandMass*l1*mHandMass*sin(theta(1) - theta(2)) + ...
        ca4*l3*m4*cos(Q25 + theta(1) - theta(2)));
    F2 = thetaDot(1)^2*(cx4*l3*m4*sin(Q25 + theta(1) - theta(2)) ...
        - ca2*l1*m2*cos(theta(1) - theta(2)) + cx2*l1*m2*sin(theta(1) - theta(2)) + ...
        cHandMass*l1*mHandMass*sin(theta(1) - theta(2)) + ca4*l3*m4*cos(Q25 + theta(1) - theta(2)));
    
    MassMat = [M11,M12;M21,M22];
    
    dampCoef = inDampCoef;
    damping = [-thetaDot(1) * dampCoef ;-thetaDot(2)*dampCoef]
    
    ceq = collocate({
        dot(theta') == thetaDot'
        MassMat*dot(thetaDot) == [F1;F2] + [torque(1) - torque(2);torque(2)];
        });
    %%% /end scary-looking equations of motion of the robot
    
    %%% Objective
    %%% Work cost
    powerSho = torque(1)*thetaDot(1) - torque(2)*thetaDot(1);
    powerElb = torque(2)*thetaDot(2);
    mechPower = [powerSho;powerElb];
    cMPSlack = mcollocate({mechPowerConEq >= mechPower
        mechPowerConEq >=0
        });
    
    costPower = sum(integrate(mechPowerConEq))*kMechanicalWorkEfficiencyMargaria/tEnd;
    %%% Force rate cost
    nnshift = 0.001;
    fraterate = dot(dot(torque));
    cFRRSlack = mcollocate({frrConEq >= fraterate
        frrConEq >= 0});
    cFRRNegSlack = mcollocate({frrConNegEq <= fraterate
        frrConNegEq <= 0});
    %costPowerForceRateLin = cForceRateRateLin*sum(integrate((fraterate+abs(fraterate))/2+nnshift))/tEnd;
    costPowerForceRateLin = cForceRateRateLin*sum(integrate(frrConEq+ -frrConNegEq))/tEnd;
    costPowerForceRateQuad = cForceRateRateQuad*sum(integrate(fraterate.^2+nnshift))/tEnd;
    switch LIN1QUAD2
        case LIN
            cForceRateRate = cForceRateRateLin;
            costPowerForceRate = costPowerForceRateLin;
        case QUAD
            costPowerForceRate = costPowerForceRateQuad;
            cForceRateRate = cForceRateRateQuad;
    end
    %% Uno force rate cost
    cUno = cUnoTop;
    cUnoForceRate = cUno*sum(integrate((dot(torque')).^2));
    %%% Resting cost
    restingMetRate = 100;%100 Watts.
    costResting = 0;%sqrt(tEnd^2+0.00001)*restingMetRate;
    %%% Reward cost
    reward = cReward * tEnd; %avoiding tEnd<0?
    objective = (costPowerForceRate + costPower + reward)*tEnd;
    coefFigTitle = cForceRateRate;
    
    
    
    %% Solve the problem
    options = struct;
    db =dbstack;
    options.name = [nameSim,'--',db.name];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cbox, cbnd, ceq, cMPSlack, cFRRSlack,cFRRNegSlack}, x0, options);
    fprintf('now running position xy start %.2f %.2f xy end %.2f %.2f\n',xyBeg(1),xyBeg(2),xyEnd(1),xyEnd(2));
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    fiopt = subs(theta, solution);
    fidopt = subs(thetaDot, solution);
    stimopt = subs(torque, solution);
    tendopt = subs(tEnd,solution);
end %end stepLevel loop

%%%get values
tendSol = subs(tEnd, solution);
EdotCostForceRate = subs(costPowerForceRate,solution);
EdotCostMechPower = subs(costPower,solution);
EdotCostResting = subs(costResting,solution);
EdotCostTotal = EdotCostForceRate+EdotCostMechPower;
EdotReward = subs(reward,solution);
times = tendSol;
ExitFlags = result.ExitFlag;
Informs = result.Inform;
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('Fdotdot power = %.4f \n',EdotCostForceRate);
fprintf('Avg positive power = %.4f\n',EdotCostMechPower);
fprintf('Resting power = %.4f\n',EdotCostResting);
%fprintf('Reward Power = %.4f\n',EdotReward);

s=[theta(1) theta(2) thetaDot(1) thetaDot(2)];
ang = 0;
f_k=result.f_k;
Exit=[result.ExitFlag result.Inform];

%%
if isnan(EdotCostForceRate)
    [EdotCostTotal, EdotCostMechPower, EdotCostForceRate, EdotReward, tendSol, vPeak, trajectories, ExitFlags,Informs]=deal(nan);
    trajectories = struct;
    [trajectories.t_plot,trajectories.theta_plot,...
        trajectories.thetadot_plot,trajectories.torque_plot, trajectories.vhand_plot] = deal(nan);
else
t_plot = linspace(0,subs(tEnd,solution),tendSol/0.01)'; % Time vector
% figure(1);
s_plot = atPoints(t_plot,subs(s,solution));
torque_plot = atPoints(t_plot,subs(torque,solution));
theta_plot=s_plot(:,[1 2]);
thetadot_plot=s_plot(:,[3 4]);

%
frrcon_plot = atPoints(t_plot,subs(frrConEq,solution));
fraterate_plot = atPoints(t_plot,subs(fraterate,solution));
mechPower_plot = atPoints(t_plot,subs(mechPower,solution));
mechPowerCon_plot = atPoints(t_plot,subs(mechPowerConEq,solution));

% this loop compute 1. elbow and hand position 2. segment velocities 3.
% powers, mostly for plotting!
for iEnergyLoop =1:length(thetadot_plot)
    q=theta_plot(iEnergyLoop,:);
    q = q(:);
    qdot = thetadot_plot(iEnergyLoop,:);
    qdot = qdot(:);
    
    powerS(iEnergyLoop) = torque_plot(iEnergyLoop,1)*qdot(1);
    powerE(iEnergyLoop) = torque_plot(iEnergyLoop,2)*(qdot(2)-qdot(1));
    
    segment1Power(iEnergyLoop) = torque_plot(iEnergyLoop,1)*qdot(1) - torque_plot(iEnergyLoop,2)*qdot(1);
    segment2Power(iEnergyLoop) = torque_plot(iEnergyLoop,2)*(qdot(2)); 
end
eTau = cumtrapz(t_plot,powerS+powerE);

vHand = zeros(size(theta_plot,1),size(theta_plot,2));
for iH = 1:length(thetadot_plot)
    posJac = @(phi,l) [ -l(1)*sin(phi(1)),-l(2)*sin(phi(2));
                         l(1)*cos(phi(1)), l(2)*cos(phi(2)) ];
    lengths = [P.L1_L;P.L2_L];%column vector
    phi = theta_plot(iH,:)';%column vector
    dphidt = thetadot_plot(iH,:)';%column vector
    vhand = posJac(phi,lengths)*dphidt;
    vHand(iH,:)=vhand';%store as row vector. annoying.
end
tanvel = sqrt(sum(vHand.*vHand,2));
vPeak = max(tanvel);

if verbose
%     figure(7);subplot(2,1,1);
%     plot(t_plot,eTau);hold on;
%     plot(t_plot,eKSum);
%     legend({'joint work','kinetic energy'});
%     subplot(2,1,2);hold on;
%     plot(t_plot,eKSum(:)-eTau(:));
%     title('kinetic minus joint work');
%     
%     fprintf('kinetic energy peak: %.2f \n',max(eKSum));
%     
    figure(8); hold on;
    plot(t_plot,sqrt(sum(vHand.*vHand,2)),'color','k');
    ylabel('Hand Velocity (m/s)'); xlabel('Time (s)');
    figure(9); hold on;
    plot(t_plot,torque_plot(:,1),'color','k');
    plot(t_plot,torque_plot(:,2),'color','k');
    ylabel('Torque (ext ref frame)');xlabel('Time (s)');
    tor = atPoints(t_plot,subs(torque,solution));
    t=t_plot;
    state = [theta_plot thetadot_plot];
    figure(10); hold on;
    plot(t_plot,theta_plot);
    ylabel('Position (m)');xlabel('Time (s)');
    figure(11);
    subplot(2,1,1);
    plot(t_plot,fraterate_plot,'b','linewidth',2);hold on;
    plot(t_plot,frrcon_plot,'r');
    legend('true','true','pos','pos');
    ylabel('force rate rate');
    subplot(2,1,2);
    plot(t_plot,mechPower_plot,'b','linewidth',2);hold on;
    plot(t_plot,mechPowerCon_plot,'r');
    legend('true','true','pos','pos');
    ylabel('mechanical power');
end

%     % forward simulation; may wish to add back
%     parms = struct;
%     parms.U(1) = spline(t_plot,stim_u(:,1));
%     parms.U(2) = spline(t_plot,stim_u(:,2));
%     parm.m3 = m3;
%     %   [tSim,stateSim]=ode45(@(tSim,stateSim)forwardMassModel(tSim,stateSim,parms),[0,tendopt],[theta_start,0,0],[]);
%

% compute the cost in joules.
EdotCostResting = 100;
ECostResting = EdotCostResting .* times;
ECostMechWork = EdotCostMechPower .* times;
ECostForceRate = EdotCostForceRate .* times;
ECostTotal = ECostMechWork + ECostForceRate;
EReward = EdotReward .*times;

trajectories = struct;
trajectories.t_plot = t_plot;
trajectories.theta_plot = theta_plot;
trajectories.thetadot_plot = thetadot_plot;
trajectories.torque_plot = torque_plot;
trajectories.vhand_plot = vhand;
toc
end