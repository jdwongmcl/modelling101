% scrPointMass
% Goal: Simplest optimal control model where we move to targets. 

x_start = [0.0;
  0.0];
x_targets = [0.0;
  0.1];
for iTargets = 1:size(x_targets,2)
  
  nodes = 10:10:30;
  for iNodes = 1:length(nodes) %LOOP THROUGH all of our node amounts. 
    if iNodes==1
      tic
    end
    disp(['Round: ',num2str(iNodes)])
    
    %%%setup the optimal control problem by defining the
    %%%1 SYSTEM TIME, STATES AND CONTROLS
    %%%2 EQUATIONS OF MOTION
    %%%3 TASK CONSTRAINTS
    %%%4 OBJECTIVE
    %%%5 INITIAL GUESSES FOR time, state, control
    
    
    %%% STEP 1: system time, states and controls.
    toms t % TIME VARIABLE
    tEnd = 1; % TIME VALUE: we're not going to optimize it/let it change.
    curNodes=nodes(iNodes);
    p = tomPhase('p', t, 0, tEnd, curNodes, [], 'spline3');
    setPhase(p);
    x = tomState('fi',2,1);             %
    dxdt = tomState('dxdt',2,1);        %
    force = tomState('force',2,1);      %
    %%%/ end step 1.
    
    %%% STEP 2: EQUATIONS OF MOTION FOR OUR SYSTEM. 
    nameSystem = 'pointMass'; %this is not programmatic. verify that your label matches what you're doing!
    F=[0; 0]; %no external forces acting on hand
    m = 1;
    MassMat = [m,0;0,m];
    
    cEquationsOfMotion = collocate({
      dot(x) == dxdt
      MassMat*dot(dxdt) == force + F;
      });
    
    %%% /END STEP 2.
    
    %%% STEP 3: TASK CONSTRAINTS.
    x_target = [x_targets(1,iTargets);x_targets(2,iTargets)];
    
    %%% Boundary constraints
    cBnd = {
      initial({
      x == x_start;
      dxdt == 0;
      dot(dxdt) == 0;
      })
      final({x == x_target;
      dxdt == 0;
      dot(dxdt) == 0;
      })
      };
    
    %%% Box constraints
    dxdtMax = 5;
    cBox = {
      collocate({dxdt <= dxdtMax
      -dxdtMax <= dxdt});
      };
    
    cBndAccel = {initial(force) == final(force); ...
      initial(dot(force)) == final(dot(force));
      
      };
    cBndAccel = {};
    %%% /END STEP 3.
        
    %%% STEP 4: Objective
    %%% Work cost
    nameObj='WorkFR';
    powerX = force(1)*dxdt(1);
    powerY = force(2)*dxdt(2);
    kMechanicalWorkEfficiencyMargaria = 4.8; %imaginary scaling: muscles are only 25% efficient.
    posMechWork = integrate(powerX + powerY);
    costPositiveWork = posMechWork*kMechanicalWorkEfficiencyMargaria;
    %%% Force rate cost
    nnshift = 0.0000001;
    cForceRateRate = 1.2;
    costForceRateQuad = cForceRateRate*sum(integrate((dot(dot(force'))).^2+nnshift));
    costForceRate = costForceRateQuad;
    cObj = .001;
    objective = cObj * (costForceRate + costPositiveWork);
    objective = 1e-7*sum(integrate(dot(dot(force')).^2))
    %%% /end step 4.
    
    %%% STEP 5: initial guess
    if iNodes==1
      % then our initial guess has to be very basic, since we don't have
      % any good information about the solution. 
      x0 = {
        %tEnd == 1 %if tEnd isn't fixed, tomlab DEMANDS we set it first.
        icollocate({
        x  == (x_start-x_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        dxdt  == (x_target-x_start)/1;%jdw hack
        })
        collocate({
        })
        };
      
      
    else
      %%% then we use the 'warm' start from coarser optmizations (involving
      %%% fewer nodes). 
      x0 = {
        %tend == tEndopt %%as above for tend.
        icollocate({
        x == x_opt
        dxdt == dxdt_opt
        })
        
        collocate({
        force == force_opt
        })
        };
    end
    %%% end step5
    
    %% Solve the problem
    options = struct;
    options.name = [nameSystem,' ',nameObj,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    
    %%%below, make sure that all of the constraints and initial guesses are
    %%%properly included!
    Prob = sym2prob(objective, {cBox, cBnd, cEquationsOfMotion, cBndAccel}, x0, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    x_opt = subs(x, solution);
    dxdt_opt = subs(dxdt, solution);
    force_opt = subs(force, solution);
    
  end %end dampingLevel loop
  
  %%%add to solution vector
  solutions(iTargets) = solution;
  %%%get values
  ECostForceRate(iTargets) = subs(costForceRate,solution);
  ECostPosMechWork(iTargets) = subs(posMechWork,solution);
  ECostPositiveWork(iTargets) = subs(costPositiveWork,solution);
  
  EdotMechPower(iTargets) = ECostPosMechWork(iTargets)/tEnd;
  EdotCostForceRate(iTargets) = subs(costForceRate,solution)./tEnd;
  EdotCostPositiveWork(iTargets) = subs(costPositiveWork,solution)./tEnd;
  times(iTargets) = tEnd;
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('TARGET %i\n',iTargets);
  fprintf('Fdotdot power = %.4f \n',EdotCostForceRate(iTargets));
  fprintf('Positive Power = %.2f \n',EdotMechPower(iTargets));
  fprintf('Avg positive power = %.4f\n',EdotCostPositiveWork(iTargets));
  
  s=[x(1) x(2) dxdt(1) dxdt(2)];
  ang = 0;%grav_angle*180/pi;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  Exits(iTargets,:) = Exit;
  %%
  t_sol = linspace(0,subs(tEnd,solution),100)'; % Time vector where we want our trajectory evaluated.
  
  s_sol = atPoints(t_sol,subs(s,solution));
  force_sol = atPoints(t_sol,subs(force,solution));
  x_sol=s_sol(:,[1 2]);
  dxdt_sol=s_sol(:,[3 4]);
  
  figure(8); hold on;
  plot(t_sol,sqrt(dxdt_sol(:,1).^2+dxdt_sol(:,2).^2));
  ylabel('tan vel'); xlabel('Time (s)');
  figure(9); hold on;
  plot(t_sol,force_sol(:,1));
  plot(t_sol,force_sol(:,2));
  ylabel('Force (N)');xlabel('Time (s)');
  tor = atPoints(t_sol,subs(force,solution));
  t=t_sol;
  state = [x_sol dxdt_sol];
  figure(10); hold on;
  plot(t_sol,x_sol);
  ylabel('Position (m)');xlabel('Time (s)');
end

cd figures
eval(['save ',[nameSystem,nameObj],' Exits ECostPositiveWork ECostForceRate EdotCostPositiveWork EdotCostForceRate times force t tEnd curNodes s x_sol dxdt_sol force_sol objective f_k Exit solution'])
toc
%%
Exits
figure(8);
savefig([nameSystem,nameObj,'_dqdt']);
figure(9);
savefig([nameSystem,nameObj,'_u']);
figure(10);
savefig([nameSystem,nameObj,'_x']);
figure(11);
savefig([nameSystem,nameObj,'_energybreakdown']);
cd ../
