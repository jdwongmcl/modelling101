function out = atkeson_fig1_targets_digitized()

%digitized atkeson reach targets
shoulderxy = [-438.812006291001; 76.34712622711231]/1000;

targets_meters = [[67.15532410445553; -63.64054981747637],...
[3.0351633146545964; -271.7116283415382],...
[-54.69967588442182; -293.06721894596814],...
[-228.29393301018177; -270.87164177776384],...
[-284.6388791874198; -66.52355454907439],...
[-301.3745436910914; 170.97018893024062],...
[-91.21595618544666; 322.5361543475311],...
[84.43555616854007; 378.33475333180587]]/1000;

%manually flip the x axis, which is the second row. 
targets_meters(1,:) = -targets_meters(1,:);
%subtract out the shoulder, which in all of our models are at [0,0];
targets_from_sho = targets_meters-shoulderxy;

movements = [[1;5],[2;6],[3;7],[4;8]];

startend = []
for i =1:size(movements,2)
    mstart = movements(1,i);
    mend = movements(2,i);
    startend(:,1,i) = targets_from_sho(:,mstart);
    startend(:,2,i) = targets_from_sho(:,mend);
    
end
out.startend = startend
out.targets = targets_from_sho;
out.movements = movements;
    
figure();

for i =1:length(out.targets)
    plot(out.targets(1,i),out.targets(2,i),'b.','markersize',5);
end