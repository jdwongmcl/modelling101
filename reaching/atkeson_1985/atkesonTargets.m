function out = atkesonTargets(xy_start,xy_target,varargin)
% Simulate reaches in a parasagittal plane, under gravity. 
% Context 1985 CG Atkeson & Hollerbach figure XX. 
% small change in reaching trajectories moving upward against gravity
% vs downwards with gravity. downwards movements go more straight down,
% sadly i don't have a strong intuition about why they bow out.
%%% processing optional args
% defaults

%paramFile = 'paramsKinarmValidated80KgSubj.mat'
%nameSim = ['Task-DistanceAndDuration-Min_FoRaTi'];
verbose = 1;% show figures =1
g = 9.81;
cForceRateRate = 8.5e-2;   %roughly matching wong cluff kuo 2020
m3 = 0;
sTimeExptHalfCycle =0.75;
nodes = [10,20, 40, 60, 120, 200]; % learned something! this really affects the energy balance.
P = getVUArmSegmentParameters();
l1 = P.l(1);
l2 = P.l(2);

for i = 1 : 2 : length(varargin)
    option = varargin{i};
    value = varargin{i + 1};
    switch option
        case 'paramfile'
            paramFile = value;
        case 'namesim'
            nameSim = value;
        case 'nodes'
            nodeLevels = value;
        case 'verbose'
            verbose = value;
        case 'handmass'
            m3 = value;
        case 'movtime'
            sTimeExptHalfCycle = value;
        case 'g'
            g = value;
        case 'coeffrr'
            cForceRateRate = 8.5e-2;   %roughly matching wong cluff kuo 2020
        case 'nodes'
            nodes = value;
        case 'l1'
            l1 = value;
        case 'l2'
            l2 = value;
        otherwise
            fprintf('warning! you tried to set %s but that is not a thing.\n',option);
    end
end

% we set the coefficient to force rate in an if statement.
cReward = 1;
cRestingRate = 1;

DISCRETE1CONTINUOUS2 = 1;

nameObjective = 'workfr';
nameTask = 'gravity';

nameSys=['armEndpoint_','Mass',num2str(m3)','kg_','g_',num2str(round(g))];
labelThisSim = ['OBJ',nameObjective,'TSK',nameTask,'SYS',nameSys] %this is not programmatic. verify that your label matches what you're doing!
fprintf('press enter to optimize:\n');
pause

%% compute the targets
% FROM JDW CLUFF KUO, the target locations and durations.

x=xy_start(1);y=xy_start(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

q_start = [q1,q2];


x=xy_target(1);y=xy_target(2);
c = sqrt(x^2+y^2);
gamma = atan2(y,x);
beta = acos((l1^2+c^2-l2^2)/(2*l1*c));
q1 = gamma - beta;
elb = acos((l2^2+l1^2-c^2)/(2*l2*l1));
q2 = pi - (elb-q1);

q_target = [q1,q2];

%%
  fi_start  = [q_start(:,:)];
  fi_target = [q_target(:,:)];
  if sum(imag([fi_start,fi_target]))>0
      fprintf('warning! you cannot reach one of the targets. exiting.\n');
  end
  
  %% calculating initial states etc...
  disp('calculated equilibrium start... now running OC problem')
  for iSteps = 1:length(nodes) %LOOP THROUGH 3 DAMPING LEVELS, which seems to help.
    if iSteps==1
      tic
    end
    disp(['Round: ',num2str(iSteps)])
    
    %%%setup the tomlab problem by defining the
    %%%1 TIME
    %%%2 PHASE
    %%%3 CONTROLS
    %%%4 STATES
    toms t %
    tHalf = sTimeExptHalfCycle;
    tend = tHalf*2;
    
    p = tomPhase('p', t, 0, tend, nodes(iSteps), [], 'spline3');
    setPhase(p);
    fi = tomState('fi',1,2);        % segment angles
    fid = tomState('fid',1,2);      % segment angular velocities
    stim = tomState('stim',1,2);  % jer: torque
    
    % slack variables
    mechPowerConPos = tomState('mechPowerConPos',1,2);
    frrConPos = tomState('frrConPos',1,2);
    frrConNeg = tomState('frrConNeg',1,2);
    %%%/setup the tomlab problem by defining the
    
    %%% initial guess
    if iSteps==1
      %initial guess
      x0 = {
        %tend == 1 % WARNING: if T IS AN OPT VARIABLE, then THIS NEEDS TO BE FIRST IN LIST. propt documentation.
        icollocate({
        fi  == (fi_start-fi_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        fid  == (fi_target-fi_start)/1;%jdw hack
        })
        collocate({
        })
        };
      
      %%% otherwise warmstart
    else
      x0 = {
        %tend == tendopt %%here we are not solving for this!!
        icollocate({
        fi == fiopt
        fid == fidopt
        })
        
        collocate({
        stim == stimopt
        })
        };
    end
    
    switch DISCRETE1CONTINUOUS2
      case 1
        DiscOrCont = 'Discrete';
        
        cdiscretecontinuous = { ...
          initial(dot(fid))' == 0
          final(dot(fid))' == 0
          atPoints(tHalf,dot(fid))==0};
        cwrap={
          };
        
        
      case 2
        DiscOrCont = 'Continuous';
        cdiscretecontinuous = {};
        cwrap = {   initial(stim)'== final(stim)'; ...
              initial(dot(stim))' == final(dot(stim))'
          };
    end
    
    %%% Boundary constraints
    cbnd = {
      initial({
      fi == fi_start;
      fid == 0;
      })
      final({fi == fi_start;
      fid == 0;
      })
      atPoints(tHalf,fi)==fi_target
      atPoints(tHalf,fid)==0;
      };
    
    %%% Box constraints
    
    %%%
    % ODEs and path constraints via virtual power
    
    m = P.m;m1 = m(1);m2=m(2);
    I = P.I;I1 = I(1);I2 = I(2);
    r = P.r;
    l = P.l;
    d = l - r;d1 = d(1);d2 = d(2);
    
    % added forces at the hand
    b = 0;
    Mjac = [ -l1*sin(fi(1)), -l2*sin(fi(2));
      l1*cos(fi(1)),  l2*cos(fi(2))];
    
    Xhand = [l1*cos(fi(1))+l2*cos(fi(2));
              l1*sin(fi(1))+l2*sin(fi(2))];

    
    Vhand = Mjac*fid';
    Fhand = [0;0];
    Fx = Fhand(1);Fy = Fhand(2);
    tauEquiv = Mjac'*Fhand;
    
    MassMat = [ I1 + d1^2*m1 + l1^2*m2 + l1^2*m3, l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3);
      l1*cos(fi(1) - fi(2))*(d2*m2 + l2*m3),          m2*d2^2 + m3*l2^2 + I2];

     F = [-l1*fid(2)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3)
           l1*fid(1)^2*sin(fi(1) - fi(2))*(d2*m2 + l2*m3)];

     G = [-g*cos(q1)*(d1*m1 + l1*m2 + l1*m3);
         -g*cos(q2)*(d2*m2 + l2*m3)];
    
     %CONSTRAINTS
     conEOM = collocate({dot(fi') == fid'
          (MassMat*dot(fid')) == (F + G + [tauEquiv(1)-tauEquiv(2); tauEquiv(2)] + [stim(1)-stim(2);stim(2)]);
    });

    %%% Objective
    powerSho = stim(1)*fid(1);
    powerElb = -stim(2)*fid(1) + stim(2)*fid(2);
    mechPower = [powerSho, powerElb];
    
    %CONSTRAINTS
    conMPSlack = mcollocate({mechPowerConPos >= mechPower
        mechPowerConPos >=0
        });
    kMechanicalWorkEfficiencyMargaria = 4.2;
    costPositiveWork = sum(integrate(mechPowerConPos))*kMechanicalWorkEfficiencyMargaria;

    %%% Force rate cost
    fraterate = dot(dot(stim));
    conFRRSlack = mcollocate({frrConPos >= fraterate
    frrConPos >= 0});

    %%% Force rate cost
    conFRRNegSlack = mcollocate({frrConNeg <= fraterate
    frrConNeg <= 0});


    costForceRate = cForceRateRate*sum(integrate(frrConPos));%+-1*frrConNeg));
    
    %%% Resting cost
    restingMetRate = 100;%100 Watts.
    costResting = sqrt(tend^2+0.00001)*restingMetRate;% approximate linear in case it's a problem.
    nameOpt = [labelThisSim,DiscOrCont];
    
    
    %%% Reward cost
    reward = 0;
    
    switch nameObjective
      case 'u2'
        objective = sum(integrate(stim.^2));
      case 'workfr' 
        objective = (costForceRate + costPositiveWork);
    end
    
    
    %% Solve the problem
    options = struct;
    options.name = [nameSys,' ',nameOpt,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cbnd, conEOM, cwrap, cdiscretecontinuous,conFRRNegSlack,conFRRSlack,conMPSlack}, x0, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    fiopt = subs(fi, solution);
    fidopt = subs(fid, solution);
    stimopt = subs(stim, solution);
    
  end %end mesh refinement / node increase
  %%
  %%%add to solution vector
  solutions = solution;
  %%%get values
  ECostForceRate = subs(costForceRate,solution);
  ECostPositiveWork = subs(costPositiveWork,solution);
  ECostResting = subs(costResting,solution);
  EReward = subs(reward,solution);
  
  EdotCostForceRate = subs(costForceRate,solution)./tend;
  EdotCostPositiveWork = subs(costPositiveWork,solution)./tend;
  EdotCostResting = subs(costResting,solution)./tend;
  EdotReward = subs(reward,solution)./tend;
  times = tend;
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('Fdotdot power = %.4f \n',EdotCostForceRate);
  fprintf('Avg positive power = %.4f\n',EdotCostPositiveWork);
  fprintf('Resting power = %.4f\n',EdotCostResting);
  fprintf('Reward Power = %.4f\n',EdotReward);
  
  s=[fi(1) fi(2) fid(1) fid(2)];
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  Exits = Exit;
  %%
  t_plot = linspace(0,subs(tend,solution),1000)'; % Time vector where we want our trajectory evaluated.
  %u_plot = collocate(t);
  % figure(1);
  s_plot = atPoints(t_plot,subs(s,solution));
  stim_u = atPoints(t_plot,subs(stim,solution));
  fiplot=s_plot(:,[1 2]);
  fidplot=s_plot(:,[3 4]);
  
  [xyhand,fhand,vxyhand] = deal(zeros(size(fiplot,1),size(fiplot,2)));
  
  for iloop =1:length(fiplot)
    qt = fiplot(iloop,:)';
    dqdt = fidplot(iloop,:)';
    Mjac = [ -l1*sin(qt(1)), -l2*sin(qt(2));
      l1*cos(qt(1)),  l2*cos(qt(2))];
    
    vxy = Mjac*dqdt;
    vxyhand(iloop,:) = vxy';
    xyhand(iloop,:) = [l1*cos(qt(1))+l2*cos(qt(2));
      l1*sin(qt(1))+l2*sin(qt(2))];
  
    Eg(iloop,1) = [d1*sin(qt(1)),l1*sin(qt(1))+d2*sin(qt(2)),l1*sin(qt(1))+l2*sin(qt(2))] * [m1;m2;m3] * g;
    fhand(iloop,:) = [0;b*(-vxy(2)+vxy(1))*sqrt((xy_target(2)-xyhand(iloop,2))^2)];
    
    jac1 = [ -d1*sin(qt(1)), 0;
              d1*cos(qt(1)), 0];
    jac2 = [ -l1*sin(qt(1)), -d2*sin(qt(2));
                l1*cos(qt(1)),  d2*cos(qt(2))];
    jac3 = [ -l1*sin(qt(1)), -l2*sin(qt(2));
              l1*cos(qt(1)),  l2*cos(qt(2))];
 
    DXY1 = jac1*dqdt;
    DXY2 = jac2*dqdt;
    DXY3 = jac3*dqdt;
    Ek(iloop,:) = 1/2 * m1 * (DXY1' * DXY1) + ...
                  1/2 * m2 * (DXY2' * DXY2) + ...
                  1/2 * m3 * (DXY3' * DXY3) + ...
                  1/2 * I1 * dqdt(1)^2 + ...
                  1/2 * I2 * dqdt(2)^2;
              
    
    PowEP(iloop,:) = sum(fhand(iloop,:).*vxyhand(iloop,:));
    PowJoints(iloop,:) = sum(stim_u(iloop,:) * dqdt - stim_u(iloop,2) * dqdt(1));
  end
  Eg = Eg-Eg(1);
  WorkEP = cumtrapz(t_plot,PowEP);
  WorkJoints = cumtrapz(t_plot,PowJoints);

  figure(8); hold on;
  c = [0,0.5,0.5]
  plot(t_plot,sqrt(vxyhand(:,1).^2+vxyhand(:,2).^2),'color',[0,0.5,0.5]);
  ylabel('Hand speed (m \cdot s^{-1})'); xlabel('Time (s)');
  figure(9); hold on;
  plot(t_plot,stim_u(:,1),'color',[0,0.5,0.5]);
  plot(t_plot,stim_u(:,2),'color',[0,0.5,0.5]);
  ylabel('Torque (N \cdot m)');xlabel('Time (s)');
  tor = atPoints(t_plot,subs(stim,solution));
  t=t_plot;
  state = [fiplot fidplot];
  figure(10); hold on;
  plot(t_plot,fiplot,'color',c);
  xlabel('Position (m)');xlabel('Time (s)');
  
toc

%% plot handpath
figure(12);
plot(xyhand(:,1),xyhand(:,2),'linewidth',2);hold on;
rs = 1:10:length(xyhand);
quiver(xyhand(rs,1),xyhand(rs,2),vxyhand(rs,1),vxyhand(rs,2));
quiver(xyhand(rs,1),xyhand(rs,2),fhand(rs,1),fhand(rs,2));
xlabel('x (m)');ylabel('y (m)');
legend('hand','speed','force');
axis equal

%% figure 13 energy balance
figure(13);
plot(t_plot,WorkJoints,'linewidth',3);hold on;
plot(t_plot,WorkEP,'linewidth',2);
plot(t_plot,Ek,'linewidth',1,'linestyle','--');
plot(t_plot,Eg,'linewidth',1,'linestyle','-.')
plot(t_plot,Ek+WorkEP+Eg,'linewidth',2);
legend({'joint work','work endpoint F','kinetic','gravitational','sumExternalWork'},'location','northwest');
%%
Exits
cd figures;
figure(8);
savefig([nameOpt,nameSys,'_dqdt']);
figure(9);
savefig([nameOpt,nameSys,'_u']);
figure(10);
savefig([nameOpt,nameSys,'_x']);
figure(11);
savefig([nameOpt,nameSys,'_energybreakdown']);
figure(12);
savefig([nameOpt,nameSys,'_quiver']);
figure(13);
savefig([nameOpt,nameSys,'_ebalance']);

cd ../

out = struct();
traj = struct;
traj.phi = fiplot;
traj.phidot = fidplot;
traj.xyhand = xyhand;
traj.vxyhand = vxyhand;
traj.t = t_plot;
traj.tor = tor;
traj.WorkJoints = WorkJoints;
traj.WorkEP = WorkEP;
traj.Ek = Ek;
traj.Eg = Eg;
out.traj = traj;
out.ECostForceRate = ECostForceRate;  
out.ECostPositiveWork=ECostPositiveWork;
out.ECostResting=ECostResting;
out.EReward=EReward; 
out.EdotCostForceRate=EdotCostForceRate ;
out.EdotCostPositiveWork=EdotCostPositiveWork; 
out.EdotCostResting=EdotCostResting; 
out.EdotReward=EdotReward;
