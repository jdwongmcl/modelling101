tgts = atkeson_fig1_targets_digitized()

%% test it in original position
clear all;
xyShoAtkeson = [-0.40,0.10];
xy_start = [0,-.30]-xyShoAtkeson;
xy_target = [0.1,.35]-xyShoAtkeson;
output(i) = atkesonTargets(xy_start,xy_target);


%% replicate target reach up and down, at different gravity
tgtpair = tgts.startend(:,:,3);%this is from target 3 to 7.
gs = linspace(0,9.81,5);
for i =1:length(gs)
    curG = gs(i);
    output(i) = atkesonTargets(tgtpair(:,1),tgtpair(:,2),'g',curG,'l2',.4);
    
end

%%

startend = tgts.startend
for i =1:size(startend,2)
    mstart = startend(:,1,i);
    mend = startend(:,2,i);
end