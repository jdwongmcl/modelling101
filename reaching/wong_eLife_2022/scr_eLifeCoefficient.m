% scrWongCluffKuoExptUsingKinarmEOM
% Goal: generate a coefficient for force-rate that produces minimum jerk
% trajectories and power costs that are the same as the experiment.
% Idea: using Kinarm EOM, compute the joint torques that minimize the net
% energetic cost of reaching, while generating power consumption that is
% very similar to our empirical data.
%
% HISTORICAL NOTES ABOUT THIS CODE.
% discovered that the model had damping at the joints in old EOM from DAK.
% discovered that this really helps the system, added them back in and turn
% them down during optimization.
%
% Answer: (acceleration constraint: 3.5e-6 --ballpark asssuming quadratic cost, min jerk motion)
% 		1.2e-4; -----ballpark quadratic cost, sinusoidal movements
%       4.0e-2; -----ballpark abs approx cost, sinusoindal movements. 
clear all;

cForceRateRateLin = 8.5e-2;%8.5e-2 is what we got from regressing our data. 
cForceRateRateQuad = 1.5e-4;

DISCRETE1CONTINUOUS2 = 2;
LIN1QUAD2 = 1;

labelThisSim = 'FROZENWorkAndQUADForceRate' %this is not programmatic. verify that your label matches what you're doing!

warning off

% load params_TOMLAB

% FROM JDW CLUFF KUO, the target locations and durations. 
fi_targets_sho = [62.0000   87.0000
  66.3376   82.6624
  68.6401   80.3599
  70.0806   78.9194
  70.9822   78.0178].*(pi/180);
sTimeExptHalfCycle =[0.8571    0.6451    0.5173    0.4286    0.3681];

% LOOP THROUGH ALL TARGETS
for iTGT = 1:length(fi_targets_sho)

  %time = durs(TGT);
  fi_start  = [fi_targets_sho(iTGT,1),fi_targets_sho(iTGT,1)+5/180*pi];
  fi_target = [fi_targets_sho(iTGT,2),fi_targets_sho(iTGT,2)+5/180*pi];

  
  %% calculating initial states etc...
  F=[0 0]; %no external forces acting on hand
  disp('calculated equilibrium start... now running OC problem')
  gradualSteps = 10:10:30;
  for iSteps = 1:length(gradualSteps) %LOOP THROUGH 3 DAMPING LEVELS, which seems to help. 
    if iSteps==1
      tic
    end
    disp(['Round: ',num2str(iSteps)])
    
    %%%setup the tomlab problem by defining the
    %%%1 TIME
    %%%2 PHASE
    %%%3 CONTROLS
    %%%4 STATES
    toms t %
    
	tHalf = sTimeExptHalfCycle(iTGT);
    tend = tHalf*2;
    steps=10;
    p = tomPhase('p', t, 0, tend, steps, [], 'spline3');
    setPhase(p);
    fi = tomState('fi',1,2);        % segment angles
    fid = tomState('fid',1,2);      % segment angular velocities
    stim = tomState('stim',1,2);  % jer: torque
    mechPowerConEq = tomState('mechPowerConEq',1,2);
    frrConEq = tomState('frrConEq',1,2);
    %%%/setup the tomlab problem by defining the
    
    %%% initial guess
    if iSteps==1
      %initial guess
      x0 = {
        %tend == 1 % WARNING: if T IS AN OPT VARIABLE, then THIS NEEDS TO BE FIRST IN LIST. propt documentation.
        icollocate({
        fi  == (fi_start-fi_target)/2;%vec(interp1([0 time],[fi_start; fi_target],t))'
        fid  == (fi_target-fi_start)/1;%jdw hack
        })
        collocate({
        %stim  == stim_start %relative muscle activation
        %         stim   == [vec(interp1([0 time],[4.5; 4.51],t/2))',vec(interp1([0 time],[-4.5; -4.51],t/2+1))']
        })
        };
      %%% otherwise warmstart
    else
      x0 = {
        %tend == tendopt %%here we are not solving for this!!
        icollocate({
        fi == fiopt
        fid == fidopt
        })
        
        collocate({
        stim == stimopt
        })
        };
    end
    
	if DISCRETE1CONTINUOUS2 == 1
		DiscOrCont = 'Discrete';
		cdiscretecontinuous = { ...
			1/10*initial(dot(fid))' == 0
			1/10*atPoints(tHalf,dot(fid))' == 0; ...
			1/10*initial(dot(fid))' == 1/10*final(dot(fid))'};
		cwrap={initial(dot(stim))' == final(dot(stim))'};
		
	else
		DiscOrCont = 'Continuous';		
		cdiscretecontinuous = {};
  	  	cwrap = {1/10*initial(stim)'==1/10*final(stim)'; ...
  	  		1/100*initial(dot(stim))' == 1/100*final(dot(stim))'
  	  	};
		
    
      switch LIN1QUAD2
        case 1
          cForceRateRate = cForceRateRateLin; %linear (abs) frate term
        case 2
          cForceRateRate = cForceRateRateQuad; %puts force rate cost in the ballpark for continuous. 
      end
		
	end
		
	
    %%% Boundary constraints
    cbnd = {
      initial({
      fi == fi_start;
      fid == 0;      
      })
      final({fi == fi_start;
      fid == 0;     
      })
      };
    
    %%% Box constraints
    cbox = {
      %       mcollocate(0 <= fi(1) <= 3*pi/2)
      %       mcollocate(-pi/4 <= fi(2)-fi(1) <= pi)
      %mcollocate(-100 <= fid <= 100)
      
      %0.005 <=  collocate(stim)  <= 1
      %0 <=tend <=10
      };
	 cmid = {      
      atPoints(tHalf,fi)' == fi_target';
      atPoints(tHalf,fid)' == zeros(2,1);      
      };
	  

	 
    %%%
    % ODEs and path constraints via virtual power
    
    P = load('paramsKinarmValidated80KgSubj.mat');
    m1 = P.L1_M;
    m2 = P.L2_M;
    m3 = P.L3_M;
    m4 = P.L4_M;
    I1 = P.L1_I;
    I2 = P.L2_I;
    I3 = P.L3_I;
    I4 = P.L4_I;
    l1 = P.L1_L;
    l3 = P.L3_L;
    cx1 = P.L1_C_AXIAL;
    ca1 = P.L1_C_ANTERIOR;
    cx2 = P.L2_C_AXIAL;
    ca2 = P.L2_C_ANTERIOR;
    cx3 = P.L3_C_AXIAL;
    ca3 = P.L3_C_ANTERIOR;
    cx4 = P.L4_C_AXIAL;
    ca4 = P.L4_C_ANTERIOR;
    Q25 = P.L2_L5_ANGLE;
    
    M11 = m1*ca1^2 + m4*ca4^2 + m1*cx1^2 + m4*cx4^2 + m2*l1^2 + I1 + I4;
    M12 = cx2*l1*m2*cos(fi(1) - fi(2)) - ca4*l3*m4*sin(Q25 + fi(1) - fi(2)) + ca2*l1*m2*sin(fi(1) - fi(2)) + cx4*l3*m4*cos(Q25 + fi(1) - fi(2));
    M21 = M12; %Kane/Lagrange derivations produce a symmetric mass matrix.
    M22 = m2*ca2^2 + m3*ca3^2 + m2*cx2^2 + m3*cx3^2 + m4*l3^2 + I2 + I3;
    
    F1 = ca2*l1*m2*fid(2)^2*cos(fi(1) - fi(2)) - cx2*l1*m2*fid(2)^2*sin(fi(1) - fi(2)) - ca4*l3*m4*fid(2)^2*cos(Q25 + fi(1) - fi(2)) - cx4*l3*m4*fid(2)^2*sin(Q25 + fi(1) - fi(2));
    F2 = cx2*l1*m2*fid(1)^2*sin(fi(1) - fi(2)) - ca2*l1*m2*fid(1)^2*cos(fi(1) - fi(2)) + ca4*l3*m4*fid(1)^2*cos(Q25 + fi(1) - fi(2)) + cx4*l3*m4*fid(1)^2*sin(Q25 + fi(1) - fi(2));
    
    MassMat = [M11,M12;M21,M22];

    ceq = collocate({
      dot(fi') == fid'
      MassMat*dot(fid') == [F1;F2] + [stim(1) - stim(2);stim(2)];
      });
    
    %%% Objective
    
    
    %%% Work cost
    powerSho = stim(1)*fid(1) - stim(2)*fid(1);
    powerElb = stim(2)*fid(2);
    mechPower = [powerSho,powerElb];
    
    cMPSlack = mcollocate({mechPowerConEq >= mechPower
            mechPowerConEq >=0
        });
    
    posMechWork = sum(integrate(mechPowerConEq));
    kMechanicalWorkEfficiencyMargaria = 4.2;
    costPositivePower = posMechWork*kMechanicalWorkEfficiencyMargaria /tend;
    
    %%% Force rate cost
    nnshift = 0.0000001;
    fraterate = dot(dot(stim));
    cFRRSlack = mcollocate({frrConEq >= fraterate
            frrConEq >= 0});
    costPowerForceRateLin = cForceRateRate*sum(integrate(frrConEq))/tend;
    costPowerForceRateQuad = cForceRateRate*sum(integrate(frrConEq.^2+nnshift))/tend;
 
    switch LIN1QUAD2
        case 1
            costForceRateMetPower = costPowerForceRateLin;
        case 2
            costForceRateMetPower = costPowerForceRateQuad;
    end
    
    objective = (costForceRateMetPower + costPositivePower)*tend;
    
    nameOpt = [labelThisSim,DiscOrCont];
    nameSim='Kinarm';
    %% Solve the problem
    options = struct;
    options.name = [nameSim,' ',nameOpt,'.m'];
    %       options.scale ='auto';
    
    %%% optimization setup
    options.PriLev = 2;
    options.Prob.SOL.optPar(35) = 100000; %iterations limit
    options.Prob.SOL.optPar(30) = 200000; %major iterations limit
    Prob = sym2prob(objective, {cbox, cbnd, ceq, cmid, cwrap, cdiscretecontinuous,cFRRSlack,cMPSlack}, x0, options);
    result = tomRun('snopt',Prob,1);
    solution = getSolution(result);
    fiopt = subs(fi, solution);
    fidopt = subs(fid, solution);
    stimopt = subs(stim, solution);
    
  end %end dampingLevel loop
  
  %%%add to solution vector
  solutions(iTGT) = solution;
  %%%get values
  EdotCostForceRate(iTGT) = subs(costForceRateMetPower,solution);
  EdotCostPositivePower(iTGT) = subs(costPositivePower,solution);
  
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
  fprintf('TARGET %i\n',iTGT);
  fprintf('Fdotdot power = %.4f \n',EdotCostForceRate(iTGT));
  fprintf('Avg positive power = %.4f\n',EdotCostPositivePower(iTGT));
  
  s=[fi(1) fi(2) fid(1) fid(2)];
  ang = 0;%grav_angle*180/pi;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  Exits(iTGT,:) = Exit;
  %%
  t_plot = linspace(0,subs(tend,solution),100)'; % Time vector where we want our trajectory evaluated.
  %u_plot = collocate(t);
  % figure(1);
  s_plot = atPoints(t_plot,subs(s,solution));
  stim_u = atPoints(t_plot,subs(stim,solution));
  fiplot=s_plot(:,[1 2]);
  fipplot=s_plot(:,[3 4]);
  % [e,h]=showmov3(fi,l,s_plot,0);
  % plotvs(h)
  % axis equal
  figure(8); hold on;
  matlabColor = get(gca,'ColorOrder');
  plot(t_plot,fipplot(:,1),'color',matlabColor(iTGT,:));
  ylabel('Ang Vel (s^{-1})'); xlabel('Time (s)');
  figure(9); hold on;
  plot(t_plot,stim_u(:,1),'color',matlabColor(iTGT,:));
  plot(t_plot,stim_u(:,2),'color',matlabColor(iTGT,:));
  ylabel('Force (N)');xlabel('Time (s)');
  tor = atPoints(t_plot,subs(stim,solution));
  t=t_plot;
  state = [fiplot fipplot];
  figure(10); hold on;
  plot(t_plot,fiplot,'color',matlabColor(iTGT,:));
  xlabel('Position (m)');xlabel('Time (s)');
  
end
%% plot forcerate+work
figure(11);
times = sTimeExptHalfCycle*2;
freqs = 1./times;
plot(freqs,EdotCostPositivePower,'marker','.','markersize',20);hold on;
plot(freqs,EdotCostForceRate,'marker','.','markersize',20);
plot(freqs,EdotCostForceRate+EdotCostPositivePower,'marker','.','markersize',20);
legend({'work','frate','net'}, 'location','northwest');

ylabel('Power (W)');
xlabel('Frequency (Hz)');

cd figures
eval(['save ',nameOpt,nameSim,' Exits EdotCostPositivePower EdotCostForceRate times stim t tend steps s fi fid objective f_k Exit solution'])


toc
%%
Exits

figure(8);
savefig([nameOpt,nameSim,'_dqdt']);
figure(9);
savefig([nameOpt,nameSim,'_u']);
figure(10);
savefig([nameOpt,nameSim,'_x']);
figure(11);
savefig([nameOpt,nameSim,'_energybreakdown']);

cd ../
