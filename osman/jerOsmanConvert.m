%% built on:
% Path Tracking Robot (Two-Phase)
% Copyright (c) 2007-2008 by Tomlab Optimization Inc.
%% todo
% 1 - path constraints (i would say collocation or box)
% 2 - link constraints
% 3 - linearly interpolate IC?
% 4 - objective is big!
%% jer's questions
%don't understand why the speed constraint is written this way.

%% Problem setup from osman

param.desiredSpeed = .4;
param.desiredStepLength = 0.7;
param.alpha = 1e-2; %force rate coefficient for force on COM
param.c = 15e-3; %osman's force rate smudge for leg force 
%%
NN_ = [10];
for iNN = 1:length(NN_)
  nn = NN_(iNN);
  toms t T_DS
  p1 = tomPhase('p1', t, 0, T_DS, nn);
  tomStates q1p1 q2p1 q3p1 q4p1 q1dotp1 q2dotp1 q3dotp1 q4dotp1
  tomStates Fp1 Fcp1 Fdotp1 Fcdotp1% these are controls, but continuous, so called 'states'.
  % State:
  %   1 = q1 stance leg angle
  %   2 = q2 l1 stance leg length
  %   3 = q3 foot angle
  %   4 = q4 foot length l2
  %   5 = F
  %   6 = Fc
  
  %   7 = q1dot stance angle vel
  %   8 = q2dot stance leg length
  %   9 = q3dot foot angle vel
  %   10 = q4dot foot leg length
  %   11 = Fdot
  %   12 = Fcdot
  %***************************
  
  %%%%%%%%%%%%%%%%
  x1p1 = -sin(q1p1)*q2p1;
  yp1 = cos(q1p1)*q2p1;
  
  footx0 = x1p1 + sin(q3p1)*q4p1;
  % footy = y - cos(q3p1)*q4p1; %<-----this is contrained to be zero
  
  %**************************************************
  xc1p1 = 0;
  %**************************************************
  
  %first find xddot and yddot as if we use cartesian coordinates
  
  xdotp1 = -cos(q1p1)*q2p1*q1dotp1 - sin(q1p1)*q2dotp1;
  ydotp1 = -sin(q1p1)*q2p1*q1dotp1 + cos(q1p1)*q2dotp1;
  
  Fxp1 = Fp1 * (x1p1-xc1p1)/q2p1; 
  Fyp1 = (Fp1 * yp1)/q2p1;
  
  powerXp1 = xdotp1*Fxp1;
  powerYp1 = ydotp1*Fyp1;
  powerFp1 = [powerXp1;powerYp1];
  
  Fcxp1 = (Fcp1 * (x1p1-footx0))/q4p1; 
  Fcyp1 =  (Fcp1 * yp1)/q4p1;
  powerFcp1 = xdotp1*Fcxp1 + ydotp1*Fcyp1;

  xddotp1 = (Fp1 * (x1p1-xc1p1))/q2p1 + (Fcp1 * (x1p1-footx0))/q4p1;   %<-----------------------now a variable
  yddotp1 = -1 + (Fp1 * yp1)/q2p1 + (Fcp1 * yp1)/q4p1;
  c1p1 = cos(q1p1);  s1p1 = sin(q1p1);

  q1ddotp1 = -(  c1p1*xddotp1 + s1p1*yddotp1 + 2*q1dotp1*q2dotp1 )/q2p1;
  q2ddotp1 = q2p1*q1dotp1^2 + yddotp1*c1p1 - xddotp1*s1p1;


  q3ddotp1 =  (-2*q3dotp1*q4dotp1 + 2*q1dotp1*q2dotp1*cos(q1p1 - q3p1) + q2ddotp1*sin(q1p1 - q3p1) + (q1ddotp1*cos(q1p1 - q3p1) - q1dotp1^2*sin(q1p1 - q3p1))*q2p1)/q4p1;
  q4ddotp1 = q2ddotp1*cos(q1p1 - q3p1) - 2*q1dotp1*q2dotp1*sin(q1p1 - q3p1) - (q1dotp1^2*cos(q1p1 - q3p1) + q1ddotp1*sin(q1p1 - q3p1))*q2p1 + q3dotp1^2*q4p1;
  
  % ODEs and path constraints
  con_eom1 = collocate({
    dot(p1,q1p1) == q1dotp1
    dot(p1,q2p1) == q2dotp1
    dot(p1,q3p1) == q3dotp1
    dot(p1,q4p1) == q4dotp1
    dot(p1,q1dotp1) == q1ddotp1
    dot(p1,q2dotp1) == q2ddotp1
    dot(p1,q3dotp1) == q3ddotp1
    dot(p1,q4dotp1) == q4ddotp1
    dot(p1,Fp1) == Fdotp1
    dot(p1,Fcp1) == Fcdotp1});
  
  
    % Objective phase 1
  epsilon = 1e-6;
  % FRate not FRATERATE
  
  con_work = mcollocate(p1,{
      powerConp1>=powerFp1
      powerConp1>=0});
  objectivep1work = powerConp1
  
  objectivep1work = integrate(( +abs(powerFp1))/2 + ...
      (powerFcp1+abs(powerFcp1))/2);
  objectivep1FR = integrate((sqrt(dot(Fdotp1)^2+ dot(Fcdotp1)^2+epsilon^2) ...
      - epsilon));
  
  %%%%%%%%%%%%%%%% /DYNAMICS PHASE1
  
  %%%%%%%%%%%%%%%% TRANSITION
  %%%%%%%%%%%%%%%% instant of double-support. states suffixed with '0p1' throughout.  
  q10p1 = initial(q1p1);
  q20p1 = initial(q2p1);
  q30p1 = initial(q3p1);
  q40p1 = initial(q4p1);
  
  q1dot0p1 = initial(q1dotp1);
  q2dot0p1 = initial(q2dotp1);
  q3dot0p1 = initial(q3dotp1);
  q4dot0p1 = initial(q4dotp1);
  
  state_x0p1 = -sin(q10p1)*q20p1;
  state_y0p1 = cos(q10p1)*q20p1;
  
  footx0p1 = state_x0p1 + sin(q30p1)*q40p1;
  footy0p1 = state_y0p1 - cos(q30p1)*q40p1;
  
  foot_velx0p1 = -cos(q10p1)*q20p1*q1dot0p1 - sin(q10p1)*q2dot0p1 + cos(q30p1)*q40p1*q3dot0p1 + sin(q30p1)*q4dot0p1;
  foot_vely0p1 = -sin(q10p1)*q20p1*q1dot0p1 + cos(q10p1)*q2dot0p1 + sin(q30p1)*q40p1*q3dot0p1-cos(q30p1)*q4dot0p1;
    
  %
  con_ds1 = {initial(p1,Fcp1)==0
    final(p1,Fp1)==0
    initial(p1,q2p1) <= 1
    initial(p1,q4p1) <= 1
    footy0p1==0
    foot_velx0p1==0
    foot_vely0p1==0    
    };
  %%%%%%%%%%%%%%%% instant of double-support. states suffixed with '0p1' throughout.  
  
  % Box constraints. Osman calls these 'path constraints'
  %F*(l1-1)); (Fc*(l2-1))
  con_boxp1 = {T_DS >=0
    collocate({Fcp1*(q4p1-1)<=0
              Fp1*(q2p1-1)<=0
              0.1 <= q2p1 <=2
              0.1 <= q4p1 <=2
               })
    }; %jer: osman used to have also: 
  %%%%%%%%%%%%%%end doublestance
  
  %%%%%%%%%%%%%% PHASE 2 single-support
  toms t T_SS %time variable and duration of single-support.
  p2 = tomPhase('p2', t, T_DS, T_SS, nn); %NOTE!! ARG3 and 4: syntax is starttime,duration not starttime,endtime
  setPhase(p2); % allows one to omit the phase argument in tomcontrol, initial, final, integrate, etc. propt documentation.
  tomStates q1p2 q2p2 q3p2 q4p2 u1p2 u2p2 u3p2 u4p2
  tomStates Fp2 Thipp2 Fhipp2 Fdotp2 Thipdotp2 Fhipdotp2 %controls
  
  %%%%%%% EOM PHASE 2
  x1p2 = -sin(q1p2)*q2p2;
  yp2 = cos(q1p2)*q2p2;
  
  xc1p2 = 0;
  
  xddotp2 = (Fp2 * (x1p2-xc1p2))/q2p2;
  yddotp2 = -1 + (Fp2 * yp2)/q2p2;
  
  %OD: c1 = cos(q1p2);  s1 = sin(q1p2); %<----------------1 big mass
  c1p2 = cos(q1p2);
  c3p2 = cos(q3p2);
  s1p2 = sin(q1p2);
  s3p2 = sin(q3p2);
  c1m3p2 = cos(q1p2 - q3p2);
  s1m3p2 = sin(q1p2 - q3p2);
  
  %OD:REMEMBER xddotp2 and xddotp2 is relative to frame 0 but you an use them here, in phase 2's foot frame
  u1dotp2 = -(  c1p2*xddotp2 + s1p2*yddotp2 + 2*u1p2*u2p2 )/q2p2;
  u2dotp2 = q2p2*u1p2^2 + yddotp2*cos(q1p2) - xddotp2*sin(q1p2);
  
  %OD: now you can calculate swing work
  u3dotp2 = - ( s3p2 - 2*c1m3p2*u1p2*u2p2 - s1m3p2*u2dotp2  + 2*u3p2*u4p2 + q2p2*(-(c1m3p2*u1dotp2) + s1m3p2*(u1p2*u1p2)))/q4p2+ Thipp2/q4p2^2; %<-------------------- for s asinlge pendulum M R^2 Thetaddot = Torque, then Thetaddot = Torque /(M R^2)
  u4dotp2 = c3p2 - 2*s1m3p2*u1p2*u2p2 + c1m3p2*u2dotp2 - q2p2*(s1m3p2*u1dotp2 + c1m3p2*(u1p2*u1p2)) + q4p2*(u3p2*u3p2) + Fhipp2; %<--------------------
  
  % ODEs and path constraints
  con_eom2 = collocate({
    dot(q1p2) == u1p2
    dot(q2p2) == u2p2
    dot(q3p2) == u3p2
    dot(q4p2) == u4p2
    dot(u1p2) == u1dotp2
    dot(u2p2) == u2dotp2
    dot(u3p2) == u3dotp2
    dot(u4p2) == u4dotp2
    dot(Fp2) == Fdotp2
    dot(Thipp2) == Thipdotp2
    dot(Fhipp2) == Fhipdotp2});
  
  %%%%%%%/ EOM PHASE2
  
  %%%%%%% LOOP CONSTRAINTS
 
 
  %the state_x and state_y of the big mass at the begining of Phase 1 *******************************
  x0phase1 = [initial(p1,q1p1);initial(p1,q2p1);initial(p1,q3p1);initial(p1,q4p1);...
    initial(p1,q1dotp1);initial(p1,q2dotp1);initial(p1,q3dotp1);initial(p1,q4dotp1);];
  
  F0phase1 =  initial(p1,Fp1); Fc0phase1 = initial(p1,Fcp1);
  
  q10phase1 = x0phase1(1); q20phase1 = x0phase1(2);  q30phase1 = x0phase1(3); q40phase1 = x0phase1(4);
  q10phase1dot = x0phase1(5); q20phase1dot = x0phase1(6);  q30phase1dot = x0phase1(7); q40phase1dot = x0phase1(8);
  F0phase1dot =  initial(p1,Fdotp1); Fc0phase1dot = initial(p1,Fcdotp1);
  
  state_x0phase1 = -sin(q10phase1)*q20phase1; state_y0phase1 = cos(q10phase1)*q20phase1;
  x0phase1dot = -cos(q10phase1)*q20phase1*q10phase1dot - sin(q10phase1)*q20phase1dot;
  y0phase1dot = -sin(q10phase1)*q20phase1*q10phase1dot + cos(q10phase1)*q20phase1dot;
  
  % the state of the foot at the end of phase 2
  footx0phase1 = state_x0phase1 + sin(q30phase1)*q40phase1;
  footy0phase1 = state_y0phase1 - cos(q30phase1)*q40phase1;
  
  foot_velx0phase1 = -cos(q10phase1)*q20phase1*q10phase1dot - sin(q10phase1)*q20phase1dot + cos(q30phase1)*q40phase1*q30phase1dot + sin(q30phase1)*q40phase1dot;
  foot_vely0phase1 = -sin(q10phase1)*q20phase1*q10phase1dot + cos(q10phase1)*q20phase1dot + sin(q30phase1)*q40phase1*q30phase1dot-cos(q30phase1)*q40phase1dot;
  
  %LINK the end of phase 1 to begining of phase 2 ******************************************************************************************************
  %at the end of phase 1, find the state_x and state_y of the big mass
  xFphase1 = [final(p1,q1p1);final(p1,q2p1);final(p1,q3p1);final(p1,q4p1);...
    final(p1,q1dotp1);final(p1,q2dotp1);final(p1,q3dotp1);final(p1,q4dotp1);];
  
  q1Fphase1 = xFphase1(1); q2Fphase1 = xFphase1(2);  q3Fphase1 = xFphase1(3); q4Fphase1 = xFphase1(4);
  
  FFphase1 =  final(p1,Fp1); FcFphase1 = final(p1,Fcp1);
  q1Fphase1dot = xFphase1(5); q2Fphase1dot = xFphase1(6);  q3Fphase1dot = xFphase1(7); q4Fphase1dot = xFphase1(8);
  FFphase1dot =  final(p1,Fdotp1); FcFphase1dot = final(p1,Fcdotp1);
  
  %the state at the begining of phase 2,
  % q1  q2   q3   q4   q1dot q2dot q3dot q4dot %% controls F   Thip  Fhip
  x0phase2 = [initial(p2,q1p2);initial(p2,q2p2);initial(p2,q3p2);initial(p2,q4p2);...
    initial(p2,u1dotp2);initial(p2,u2dotp2);initial(p2,u3dotp2);initial(p2,u4dotp2);];
  
  q10phase2 = x0phase2(1); q20phase2 = x0phase2(2);       q30phase2 = x0phase2(3);    q40phase2 = x0phase2(4);
  F0phase2 =  initial(p2,Fp2);  %Thip0phase2 =  x0phase2(6); Fhip0phase2 =  x0phase2(7);
  q10phase2dot = x0phase2(5); q20phase2dot = x0phase2(6); q30phase2dot = x0phase2(7); q40phase2dot = x0phase2(8);
  
  F0phase2dot =  initial(p2,Fdotp2);  %Thip0phase2dot =  x0phase2(13);   Fhip0phase2dot =  x0phase2(14);
  
  %link them
  con_limitcycle = {q10phase2==q3Fphase1
    q20phase2==q4Fphase1
    q30phase2==q1Fphase1
    q40phase2==q2Fphase1
    q10phase2dot==q3Fphase1dot
    q20phase2dot==q4Fphase1dot
    q30phase2dot==q1Fphase1dot
    q40phase2dot==q2Fphase1dot
    F0phase2dot==FcFphase1dot
    F0phase2==FcFphase1};

  xFphase2 = [final(p2,q1p2);final(p2,q2p2);final(p2,q3p2);final(p2,q4p2);...
    final(p2,u1dotp2);final(p2,u2dotp2);final(p2,u3dotp2);final(p2,u4dotp2);];
  
  q1Fphase2 = xFphase2(1); q2Fphase2 = xFphase2(2);  q3Fphase2 = xFphase2(3); q4Fphase2 = xFphase2(4);
  FFphase2 =  final(p2,Fp2);  %ThipFphase2 =  xFphase2(6);  FhipFphase2 =  xFphase2(7);
  q1Fphase2dot = xFphase2(5); q2Fphase2dot = xFphase2(6); q3Fphase2dot = xFphase2(7); q4Fphase2dot = xFphase2(8) ;
  FFphase2dot =  final(p2, Fdotp2);  %ThipFphase2dot =  xFphase2(13);    FhipFphase2dot =  xFphase2(14);
  
  % the state of the mass at the end of phase 2
  state_xFphase2 = -sin(q1Fphase2)*q2Fphase2;
  state_yFphase2 = cos(q1Fphase2)*q2Fphase2;
  xFphase2dot = -cos(q1Fphase2)*q2Fphase2*q1Fphase2dot - sin(q1Fphase2)*q2Fphase2dot;
  yFphase2dot = -sin(q1Fphase2)*q2Fphase2*q1Fphase2dot + cos(q1Fphase2)*q2Fphase2dot;
  
  % link the mass and the final force
  state_xFtoOriginphase2= state_xFphase2 + footx0phase1; %<----------now relative to origin
  
  con_connectOsman3 = {
    state_xFtoOriginphase2 == (state_x0phase1+footx0phase1)
    state_yFphase2==state_y0phase1
    xFphase2dot==x0phase1dot
    yFphase2dot == y0phase1dot
    FFphase2==F0phase1
    FFphase2dot==F0phase1dot}; %<-------add derivative
  % ceq3 = [state_yFphase2 - state_y0phase1, [xFphase2dot yFphase2dot]-[x0phase1dot y0phase1dot],...
  % FFphase2-F0phase1, FFphase2dot-F0phase1dot]; %<-------add derivative
  
  %link the swing leg
  % the state of the foot at the end of phase 2
  footxFphase2 = state_xFtoOriginphase2+ sin(q3Fphase2)*q4Fphase2; %<----------now relative to origin
  footyFphase2 = state_yFphase2 - cos(q3Fphase2)*q4Fphase2;
  
  foot_velxFphase2 = -cos(q1Fphase2) * q2Fphase2*q1Fphase2dot - sin(q1Fphase2)*q2Fphase2dot + cos(q3Fphase2)*q4Fphase2*q3Fphase2dot + sin(q3Fphase2)*q4Fphase2dot;
  foot_velyFphase2 = -sin(q1Fphase2) * q2Fphase2*q1Fphase2dot + cos(q1Fphase2)*q2Fphase2dot + sin(q3Fphase2)*q4Fphase2*q3Fphase2dot-cos(q3Fphase2)*q4Fphase2dot;
  
  %linf the foot state
  % ceq4 = [q3Fphase2-q30phase1, q4Fphase2-q40phase1, q3Fphase2dot-q30phase1dot, q4Fphase2dot-q40phase1dot];
  con_connectOsman4 = {footxFphase2 == (footx0phase1+footx0phase1)
    footyFphase2 == footy0phase1
    foot_velxFphase2 == foot_velx0phase1
    foot_velyFphase2 == foot_vely0phase1};
  
  T_F = T_SS+T_DS;
  con_desiredSpeed = {(state_xFtoOriginphase2-state_x0phase1) == param.desiredSpeed*T_F};
  
  %%%%%/ LOOP AND SPEED CONSTRAINTS
  
  %%% power
  
  xdotp2 = -cos(q1p2)*q2p2*u1p2 - sin(q1p2)*u2p2;
  ydotp2 = -sin(q1p2)*q2p2*u1p2 + cos(q1p2)*u2p2;
  
  Fxp2 = (Fp2 * (x1p2-xc1p2))/q2p2;
  Fyp2 = (Fp2 * yp2)/q2p2;
  powerFp2 = xdotp2*Fxp2 + ydotp2*Fyp2;
  
  % constraint during p2
  con_boxp2 = {T_SS>=0
    collocate({Fp2*(q2p2-1)<=0
        0.1 <= q4p2 <= 1
        0.1 <= q2p2 <= 1 
  })
  };
  
  con_stepLength = {footx0p1 == param.desiredStepLength};
  
  %%%%%%% LOOP CONSTRAINTS
  
  
  %%%%%%% OBJECTIVE
  epsilon = 1e-6;
  
  objectivep2work = integrate((powerFp2+abs(powerFp2))/2);
  
  %below, osman squares his fr cost for both the body and leg.  
  objectivep2FRBody = integrate(sqrt(dot(Fdotp2)^2+epsilon^2)-epsilon);
  objectivep2FRLeg = integrate(sqrt(Thipdotp2^2+...
    Fhipdotp2^2+epsilon^2)-epsilon);
  
  % Objective
  %output.objective = (1- param.alpha) * integralCost_Jw + ...
%   param.alpha * (integralCost_Jf)^(1/param.p) + ...
%   param.c * (integralCost_swing_Jf)^(1/param.p); %   ORIGINAL
   objective = ...
     (1-param.alpha) * (objectivep1work + objectivep2work); %+ ... %% this is close to 1.
     % (param.alpha) * (objectivep1FR + objectivep2FRBody) +...
     % param.c * objectivep2FRLeg;
  %%%%%%% /OBJECTIVE
  
  %% initial guess
  
  
  USEOSMAN = 1;
  if iNN==1 && USEOSMAN
    oD = load('osmanData20200609.mat');
    X0 = {T_DS==oD.p1data.t(end);
      T_SS== oD.p2data.t(end)-oD.p2data.t(1)
      %T_F==T_DS+T_SS;
      
      %icollocate expands the setting to all
      icollocate({
      [q1p1;q2p1;q3p1;q4p1;q1dotp1;q2dotp1;q3dotp1;q4dotp1] == ...
      vec(interp1([oD.p1data.t(1),oD.p1data.t(end)],...
      [oD.p1data.state(1,:);oD.p1data.state(end,:)],t))
      
      [q1p2;q2p2;q3p2;q4p2;u1p2;u2p2;u3p2;u4p2] == ...
      vec(interp1([oD.p2data.t(1),oD.p2data.t(end)],...
      [oD.p2data.state(1,:);oD.p2data.state(end,:)],t))
      
      [Fp1;Fcp1] == ...
      vec(interp1([oD.p2data.t(1),oD.p2data.t(end)],...
      [oD.p1data.control(1,:);oD.p1data.control(end,:)],t))
      
      [Fp2;Thipp2;Fhipp2] == ...
      vec(interp1([oD.p2data.t(1),oD.p2data.t(end)],...
      [oD.p2data.control(1,:);oD.p2data.control(end,:)],t))
      });};
  elseif iNN==1 && ~USEOSMAN
          X0={T_DS==1
      T_SS== 1
            
      %icollocate expands the setting to all
      icollocate({
      [q1p1;q2p1;q3p1;q4p1;q1dotp1;q2dotp1;q3dotp1;q4dotp1] == ...
      ones(8,1)*0.3;
      
      [q1p2;q2p2;q3p2;q4p2;u1p2;u2p2;u3p2;u4p2] == ...
      ones(8,1)*0.3;
      
      [Fp1;Fcp1] == ...
      ones(2,1);
      
      [Fp2;Thipp2;Fhipp2] == ...
      ones(3,1)})
      };

  elseif iNN > 1
    XO = {T_DS == subs(T_DS,solution)
      };
  end
  
  
  %% Solve the problem
  options = struct;
  options.name = 'Osman replication';
  options.PriLev = 2;
  options.Prob.SOL.optPar(35) = 100000; %iterations limit
  options.Prob.SOL.optPar(30) = 200000; %major iterations limit
  fprintf('leg length restrictions; removed l1s and l2s as redundant. \n using osman. \n');pause;
  constraints = {con_boxp1, con_eom1, con_boxp2, con_eom2, ...
    con_connectOsman3, con_connectOsman4, con_limitcycle, con_stepLength, con_desiredSpeed};%con_desiredSpeed};%, con_connectOsman4, con_limitcycle, con_desiredSpeed};
  Prob = sym2prob(objective, constraints, X0, options);
  result = tomRun('snopt',Prob,1);
  solution = getSolution(result);
  
  
  tResult = linspace(0,subs(T_F,solution),1000);
  statep1 = [q1p1,q2p1 q3p1 q4p1 q1dotp1 q2dotp1 q3dotp1 q4dotp1];
  tp1 = subs(collocate(p1,t),solution);
  s1R = subs(collocate(p1,statep1),solution);
  statep2 = [q1p2,q2p2 q3p2 q4p2 u1p2,u2p2 u3p2 u4p2];
  s2R = subs(collocate(p2,statep2),solution);
  tp2 = subs(collocate(p2,t),solution);
  allStates = [s1R;
    s2R(:,3), s2R(:,4), s2R(:,1),s2R(:,2), s2R(:,7),s2R(:,8),s2R(:,5),s2R(:,6)];
  allT = [tp1;tp2];
end

% x1 = subs(collocate(p1,x1p1),solution);
% x1 = [x1;subs(collocate(p2,x1p2),solution)];
% x2 = subs(collocate(p1,x2p1),solution);
% x2 = [x2;subs(collocate(p2,x2p2),solution)];
% x3 = subs(collocate(p1,x3p1),solution);
% x3 = [x3;subs(collocate(p2,x3p2),solution)];
% x4 = subs(collocate(p1,x4p1),solution);
% x4 = [x4;subs(collocate(p2,x4p2),solution)];
% u1 = subs(collocate(p1,u1p1),solution);
% u1 = [u1;subs(collocate(p2,u1p2),solution)];
% u2 = subs(collocate(p1,u2p1),solution);
% u2 = [u2;subs(collocate(p2,u2p2),solution)];
%
% %% Plot result
% subplot(2,1,1);
% plot(t,x1,'*-',t,x2,'*-',t,x3,'*-',t,x4,'*-');
% legend('x1','x2','x3','x4');
% title('Path Tracking Robot state variables');
%
% subplot(2,1,2);
% plot(t,u1,'*-',t,u2,'*-');
% legend('u1','u2');
% title('Path Tracking Robot control variables');