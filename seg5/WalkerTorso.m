classdef WalkerTorso < handle
  properties (Access = public)
  end
  properties (Constant)
    nq = 5;
    B =  [  -1  0  0  0;
      1 -1  0  0;
      0  0 -1  1;
      0  0  0 -1;
      0  1  1  0];
    gamma = 0; % decline. i.e., '-' is incline.
    %   gamma = -5/180*pi;
    g = 1;
    Mtorso = 0.68;
    Ms = .08;
    Mt = .08;
    
    %   L = 1;
    C = 0.25;
    %   Ip = 0;
    Il = 0.017004160000000;
    ls = 0.5;
    lt = 0.5;
    Is = WalkerTorso.Il/4;
    It = WalkerTorso.Il/4;
    Itorso = 0.0474;  % Dimensionless via Bobbert: b.sk.j(4)/sum(b.sk.mass)
    rtorso = 0.3;     % Bobbert: COM torso is 0.29 m
    
  end
  methods
    
  end
  
  
  methods(Static)            % ------STATIC------ %
    function P = getProperties
      P = struct;
      P.nq = WalkerTorso.nq;
      P.B = WalkerTorso.B;
      P.gamma = WalkerTorso.gamma;
      P.g = WalkerTorso.g;
      P.Mtorso = WalkerTorso.Mtorso;
      P.Ms = WalkerTorso.Ms;
      P.Mt = WalkerTorso.Mt;
      
      %   L = 1;
      P.C = WalkerTorso.C;
      %   Ip = 0;
      P.Il = WalkerTorso.Il;
      P.ls = WalkerTorso.ls;
      P.lt = WalkerTorso.lt;
      P.Is = WalkerTorso.Is;
      P.It = WalkerTorso.It;
      P.Itorso = WalkerTorso.Itorso ;  % Dimensionless via Bobbert: b.sk.j(4)/sum(b.sk.mass)
      P.rtorso = WalkerTorso.rtorso;     % Bobbert: COM torso is 0.29 m
      
    end
    
    % function q1234 = setPose(Q1,Q2,k)
    % define the two angles of the whole leg, walk2 style (Q1 and Q2 are
    % leading and trailing leg).
    % this computes all 4 qus for the walker.
    % function q1234 = setPose(Q1,p)
    % k is knee flexion angle.
    % return pose of the body.
    function q1234 = setPose(Q1,Q2,k)
      p = WalkerTorso.getProperties();
      ls = p.ls;
      lt = p.lt;
      
      lOP = lt^2+ls^2 - 2*lt*ls*cos(k);
      alpha1 = acos((lt^2 - lOP^2 - ls^2)/(-2*lOP*ls));
      
      q1 = Q1 - alpha1;
      Bandalpha2 = pi/2-Q1;
      alpha2 = acos((ls^2-lOP^2-lt^2)/(-2*lOP*lt));
      B = Bandalpha2 - alpha2;
      q2 = pi/2 - B;
      
      hPR = lOP*cos(Q1);
      lPR = hPR/cos(Q2);
      alpha4 = acos((lt^2 - ls^2 - lPR^2 ) / (-2*ls*lPR));
      alpha4 = -alpha4;
      q4 = alpha4 + Q2;
      alpha3 = acos((ls^2 - lt^2 - lPR^2) / (-2*lt*lPR));
      q3 = Q2 + alpha3;
      
      q1234 =[q1;q2;q3;q4];
      XY = [-ls*sin(q1) - lt*sin(q2) + lt*sin(q3) + (ls)*sin(q4);
        ls*cos(q1) + lt*sin(q2) - lt*cos(q3) - (ls)*cos(q4);];
    end
    
    function testSetPose(qs,p)
      phi1 = qs(1);
      phi2 = qs(2);
      phi3 = qs(3);
      phi4 = qs(4);
      
      lt = p.lt; ls = p.ls;
      C = .25
      
      %%
      XYS = [-C*sin(phi1);
        C*cos(phi1);
        -ls*sin(phi1) - C*sin(phi2);
        ls*cos(phi1) + C*cos(phi2);
        -ls*sin(phi1) - lt*sin(phi2) + (lt-C)*sin(phi3);
        ls*cos(phi1) + lt*cos(phi2) - (lt-C)*cos(phi3);
        -ls*sin(phi1) - lt*sin(phi2) + lt*sin(phi3) + (ls-C)*sin(phi4);
        ls*cos(phi1) + lt*cos(phi2) - lt*cos(phi3) - (ls-C)*cos(phi4);];
      
      X = XYS(1:2:end);
      Y = XYS(2:2:end);
      
      kneeHipFoot= [0;
        0
        -ls*sin(phi1);
        ls*cos(phi1);
        -ls*sin(phi1) - lt*sin(phi2);
        ls*cos(phi1) + lt*cos(phi2);
        -ls*sin(phi1) - lt*sin(phi2) + (lt)*sin(phi3);
        ls*cos(phi1) + lt*cos(phi2) - (lt)*cos(phi3);
        -ls*sin(phi1) - lt*sin(phi2) + lt*sin(phi3) + (ls)*sin(phi4);
        ls*cos(phi1) + lt*cos(phi2) - lt*cos(phi3) - (ls)*cos(phi4);];
      kneeHipFootX = kneeHipFoot(1:2:end);
      kneeHipFootY = kneeHipFoot(2:2:end);
      figure;
      plot(X,Y,'bo','linewidth',2);
      hold on;
      plot(kneeHipFootX,kneeHipFootY,'ro','linewidth',2);
      
      axis([-.75,.75,-.1,1.4]);
    end
    
    
    function stated=odeWalkerTorso(t,state,P)
      Pconstants = WalkerTorso.getProperties();
      nq = Pconstants.nq;
      B = Pconstants.B;
      gamma = Pconstants.gamma;
      g = Pconstants.g;
      Mtorso = Pconstants.Mtorso;
      Ms = Pconstants.Ms;
      Mt = Pconstants.Mt;
      C = Pconstants.C;
      Il = Pconstants.Il;
      ls = Pconstants.ls;
      lt = Pconstants.lt;
      Is = Pconstants.Is;
      It = Pconstants.It;
      Itorso = Pconstants.Itorso ;
      rtorso = Pconstants.rtorso;
      
      taus = ppvalWrap(t,P.U);
      
      q1 = state(1);
      q2 = state(2);
      q3 = state(3);
      q4 = state(4);
      q5 = state(5);
      
      q1dot = state(6);
      q2dot = state(7);
      q3dot = state(8);
      q4dot = state(9);
      q5dot = state(10);
      
      
      MassMat = [   Is + C^2*Ms + Mtorso*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(q1 - q2)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),     -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*ls*cos(q1 - q4)*(C - ls), Mtorso*ls*rtorso*cos(q1 - q5);
        ls*cos(q1 - q2)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),      It + C^2*Mt + Mtorso*lt^2 + Ms*lt^2 + Mt*lt^2,     -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*lt*cos(q2 - q4)*(C - ls), Mtorso*lt*rtorso*cos(q2 - q5);
        -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),        -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt), It + C^2*Mt + Ms*lt^2 + Mt*lt^2 - 2*C*Mt*lt,      -Ms*lt*cos(q3 - q4)*(C - ls),                         0;
        Ms*ls*cos(q1 - q4)*(C - ls),                    Ms*lt*cos(q2 - q4)*(C - ls),                -Ms*lt*cos(q3 - q4)*(C - ls), Ms*C^2 - 2*Ms*C*ls + Ms*ls^2 + Is,                         0;
        Mtorso*ls*rtorso*cos(q1 - q5),                      Mtorso*lt*rtorso*cos(q2 - q5),                                           0,                                 0,      Mtorso*rtorso^2 + Itorso;];
      
      
      F =  [  Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2) - Ms*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + Ms*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mtorso*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mtorso*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2);
        Ms*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mtorso*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mtorso*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - C*Mt*sin(q2)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + C*Mt*cos(q2)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2);
        Ms*lt*sin(q3)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Ms*lt*cos(q3)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*sin(q3)*(C - lt)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) + Mt*cos(q3)*(C - lt)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2);
        Ms*cos(q4)*(C - ls)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*sin(q4)*(C - ls)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2);
        Mtorso*rtorso*cos(q5)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - Mtorso*rtorso*sin(q5)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2)];
      
      G = [ C*Ms*g*cos(gamma)*sin(q1) + C*Ms*g*cos(q1)*sin(gamma) + Mtorso*g*ls*cos(gamma)*sin(q1) + Mtorso*g*ls*cos(q1)*sin(gamma) + Ms*g*ls*cos(gamma)*sin(q1) + Ms*g*ls*cos(q1)*sin(gamma) + 2*Mt*g*ls*cos(gamma)*sin(q1) + 2*Mt*g*ls*cos(q1)*sin(gamma);
        C*Mt*g*cos(gamma)*sin(q2) + C*Mt*g*cos(q2)*sin(gamma) + Mtorso*g*lt*cos(gamma)*sin(q2) + Mtorso*g*lt*cos(q2)*sin(gamma) + Ms*g*lt*cos(gamma)*sin(q2) + Ms*g*lt*cos(q2)*sin(gamma) + Mt*g*lt*cos(gamma)*sin(q2) + Mt*g*lt*cos(q2)*sin(gamma);
        Mt*g*cos(gamma)*sin(q3)*(C - lt) - Ms*g*lt*cos(q3)*sin(gamma) - Ms*g*lt*cos(gamma)*sin(q3) + Mt*g*cos(q3)*sin(gamma)*(C - lt);
        Ms*g*cos(gamma)*sin(q4)*(C - ls) + Ms*g*cos(q4)*sin(gamma)*(C - ls);
        Mtorso*g*rtorso*cos(gamma)*sin(q5) + Mtorso*g*rtorso*cos(q5)*sin(gamma);];
      
      
      % note about torques:
      % since the joints are knee1 hip1 hip2 knee2 torso,
      %       B =  [  -1  0  0  0;
      %         1 -1  0  0;
      %         0  0 -1  1;
      %         0  0  0 -1;
      %         0  1  1  0];
      
      
      if length(taus) ~= length(taus(:)) % then t isn't scalar
        fprintf('warning: t is not scalar');
      end
      taus = taus(:);
      
      stated = [q1dot;q2dot;q3dot;q4dot;q5dot;
        MassMat\ (F + G + B*taus);];
    end
    function E = energy(t,state,P)
      Pconstants = WalkerTorso.getProperties();
      nq = Pconstants.nq;
      B = Pconstants.B;
      gamma = Pconstants.gamma;
      g = Pconstants.g;
      Mtorso = Pconstants.Mtorso;
      Ms = Pconstants.Ms;
      Mt = Pconstants.Mt;
      C = Pconstants.C;
      Il = Pconstants.Il;
      ls = Pconstants.ls;
      lt = Pconstants.lt;
      Is = Pconstants.Is;
      It = Pconstants.It;
      Itorso = Pconstants.Itorso ;
      rtorso = Pconstants.rtorso;
      
      tausAll = ppvalWrap(t,P.U);
      for i =1:size(state,1)
        q1 = state(i,1);
        q2 = state(i,2);
        q3 = state(i,3);
        q4 = state(i,4);
        q5 = state(i,5);
        
        q1dot = state(i,6);
        q2dot = state(i,7);
        q3dot = state(i,8);
        q4dot = state(i,9);
        q5dot = state(i,10);
        
        qdot = [q1dot;q2dot;q3dot;q4dot;q5dot];
        Jek1 = [-C*cos(q1), 0, 0, 0, 0;
          -C*sin(q1), 0, 0, 0, 0];
        
        Jek2 = [ -ls*cos(q1), -C*cos(q2), 0, 0, 0;
          -ls*sin(q1), -C*sin(q2), 0, 0, 0];
        
        Jek3 = [ -ls*cos(q1), -lt*cos(q2), -cos(q3)*(C - lt), 0, 0;
          -ls*sin(q1), -lt*sin(q2), -sin(q3)*(C - lt), 0, 0];
        
        Jek4 = [ -ls*cos(q1), -lt*cos(q2), lt*cos(q3), -cos(q4)*(C - ls), 0;
          -ls*sin(q1), -lt*sin(q2), lt*sin(q3), -sin(q4)*(C - ls), 0];
        
        Jek5 = [ -ls*cos(q1), -lt*cos(q2), 0, 0, -rtorso*cos(q5);
          -ls*sin(q1), -lt*sin(q2), 0, 0, -rtorso*sin(q5)];
        
        Masses = [Ms;Mt;Mt;Ms;Mtorso];
        Ekl = 1/2* (...
          Masses(1)*(Jek1*qdot)' * (Jek1*qdot) + ...
          Masses(2)*(Jek2*qdot)' * (Jek2*qdot) + ...
          Masses(3)*(Jek3*qdot)' * (Jek3*qdot) + ...
          Masses(4)*(Jek4*qdot)' * (Jek4*qdot) + ...
          Masses(5)*(Jek5*qdot)' * (Jek5*qdot));
        
        Irots = [Is;It;It;Is;Itorso];
        Ekr = 1/2 * sum(Irots.*qdot.^2);
        
        Eg= Masses(1) * g * C*cos(q1) + ...
          Masses(2) * g * (ls*cos(q1) + C*cos(q2)) + ...
          Masses(3) * g * (ls*cos(q1) + lt*cos(q2) - (lt-C)*cos(q3)) + ...
          Masses(4) * g * (ls*cos(q1) + lt*cos(q2) - lt*cos(q3) - (ls-C)*cos(q4)) + ...
          Masses(5) * g * (ls*cos(q1) + lt*cos(q2) + rtorso*cos(q5));
        
        Egvec(i) = Eg;
        Ekvec(i) = Ekl+Ekr;
        kneeTau1 = tausAll(i,1);
        hipTau1  = tausAll(i,2);
        hipTau2  = tausAll(i,3);
        kneeTau2 = tausAll(i,4);
        
        powerKnee1(i) = kneeTau1.*(q2dot-q1dot);% positive thigh , negative shank
        powerHip1(i) =   hipTau1.*(q5dot-q2dot);% positive torso , negative thigh1
        powerHip2(i) =   hipTau2.*(q5dot-q3dot);% positive torso , negative thigh2
        powerKnee2(i) = kneeTau2.*(q3dot-q4dot);% positive thigh2, negative shank
        powerTotal(i) = powerKnee1(i) + powerHip1(i) + powerHip2(i) + powerKnee2(i);
      end
      
      E = struct;
      E.EMech = Egvec + Ekvec;
      E.Eg = Egvec;
      E.Ek = Ekvec;
      E.powKnee1 = powerKnee1;
      E.powHip1 = powerHip1;
      E.powHip2 = powerHip2;
      E.powKnee2 = powerKnee2;
      if size(state)>1
        E.workTau = cumtrapz(t,powerTotal);
      end
      E.powerAll = [powerKnee1(:),powerHip1(:),powerHip2(:),powerKnee2(:)];
    end
    
    function [qdotColPost,ePreIntPost] = collision(state,Pushoff)
      Pconstants = WalkerTorso.getProperties();
      nq = Pconstants.nq;
      B = Pconstants.B;
      gamma = Pconstants.gamma;
      g = Pconstants.g;
      Mtorso = Pconstants.Mtorso;
      Ms = Pconstants.Ms;
      Mt = Pconstants.Mt;
      C = Pconstants.C;
      Il = Pconstants.Il;
      ls = Pconstants.ls;
      lt = Pconstants.lt;
      Is = Pconstants.Is;
      It = Pconstants.It;
      Itorso = Pconstants.Itorso ;
      rtorso = Pconstants.rtorso;
      
      q1E = state(1);
      q2E = state(2);
      q3E = state(3);
      q4E = state(4);
      q5E = state(5);
      qPre = [q1E;q2E;q3E;q4E;q5E];
      qdotColPre = [state(6:10),0,0]';
      
      %%%% system-specific
      MassMatCol=[...
        Is + C^2*Ms + Mtorso*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(q1E - q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),   -ls*cos(q1E - q3E)*(Ms*lt + Mt*lt - C*Mt),     Ms*ls*cos(q1E - q4E)*(C - ls), Mtorso*ls*rtorso*cos(q1E - q5E), -cos(q1E)*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms), -sin(q1E)*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms);
        ls*cos(q1E - q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),        It + C^2*Mt + Mtorso*lt^2 + Ms*lt^2 + Mt*lt^2,   -lt*cos(q2E - q3E)*(Ms*lt + Mt*lt - C*Mt),     Ms*lt*cos(q2E - q4E)*(C - ls), Mtorso*lt*rtorso*cos(q2E - q5E),   -cos(q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),   -sin(q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt);
        -ls*cos(q1E - q3E)*(Ms*lt + Mt*lt - C*Mt),        -lt*cos(q2E - q3E)*(Ms*lt + Mt*lt - C*Mt), It + C^2*Mt + Ms*lt^2 + Mt*lt^2 - 2*C*Mt*lt,    -Ms*lt*cos(q3E - q4E)*(C - ls),                           0,            cos(q3E)*(Ms*lt + Mt*lt - C*Mt),            sin(q3E)*(Ms*lt + Mt*lt - C*Mt);
        Ms*ls*cos(q1E - q4E)*(C - ls),                    Ms*lt*cos(q2E - q4E)*(C - ls),              -Ms*lt*cos(q3E - q4E)*(C - ls), Ms*C^2 - 2*Ms*C*ls + Ms*ls^2 + Is,                           0,                      -Ms*cos(q4E)*(C - ls),                      -Ms*sin(q4E)*(C - ls);
        Mtorso*ls*rtorso*cos(q1E - q5E),                      Mtorso*lt*rtorso*cos(q2E - q5E),                                           0,                                 0,        Mtorso*rtorso^2 + Itorso,                        -Mtorso*rtorso*cos(q5E),                        -Mtorso*rtorso*sin(q5E);
        -cos(q1E)*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms),         -cos(q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),             cos(q3E)*(Ms*lt + Mt*lt - C*Mt),             -Ms*cos(q4E)*(C - ls),         -Mtorso*rtorso*cos(q5E),                           Mtorso + 2*Ms + 2*Mt,                                          0;
        -sin(q1E)*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms),         -sin(q2E)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),             sin(q3E)*(Ms*lt + Mt*lt - C*Mt),             -Ms*sin(q4E)*(C - ls),         -Mtorso*rtorso*sin(q5E),                                          0,                           Mtorso + 2*Ms + 2*Mt];
      
      Jlambda_impulse = [0,0,0,0,0,1,0;
        0,0,0,0,0,0,1];
      Xhip = -ls*sin(q1E) - C*sin(q2E);
      Yhip = ls*cos(q1E) + C*cos(q2E);
      angleHip = atan2(Yhip, Xhip); %through the hip.
      
      if nargin == 2 %then we have a pushoff
        delta_q = MassMatCol \ Jlambda_impulse'* [sin(angleHip);cos(angleHip)]*Pushoff;
        qdotColInt = qdotColPre+delta_q;
      else
        qdotColInt = qdotColPre;
      end
      % assuming the collision takes place at the end of the rear foot.
      Jlambda = [ -ls*cos(q1E), -lt*cos(q2E), lt*cos(q3E), ls*cos(q4E), 0, 1, 0;
        -ls*sin(q1E), -lt*sin(q2E), lt*sin(q3E), ls*sin(q4E), 0, 0, 1];
      
      epsilon = 0;
      %%%% /system-specific
      
      %%%% generic
      cdot = Jlambda*qdotColInt;
      nc = length(cdot);
      
      MlambdaNI = MassMatCol \ Jlambda'; %'NI' is NotInverse; match Remy's convention.
      MlambdaNI = Jlambda * MlambdaNI;
      impulse = -1* MlambdaNI \ ((eye(nc,nc)+eye(nc,nc)*epsilon)*cdot);
      qdotColPost = qdotColInt + MassMatCol \ Jlambda' * impulse;
      %%%% generic
    end
    
    % simple function to animate given qs
    %function animate(q_sol)
    function animate(q_sol)
      q1_sol = q_sol(:,1);
      q2_sol = q_sol(:,2);
      q3_sol = q_sol(:,3);
      q4_sol = q_sol(:,4);
      q5_sol = q_sol(:,5);
      
      P = WalkerTorso.getProperties();
      ls = P.ls;
      lt = P.lt;
      rtorso = P.rtorso;
      figure();
      for i =1:50:length(q1_sol)
        q1 = q1_sol(i);
        q2 = q2_sol(i);
        q3 = q3_sol(i);
        q4 = q4_sol(i);
        q5 = q5_sol(i);
        
        joints = [0;
          0;
          -ls*sin(q1);
          ls*cos(q1);
          -ls*sin(q1) - lt*sin(q2);
          ls*cos(q1) + lt*cos(q2);
          -ls*sin(q1) - lt*sin(q2) - rtorso*sin(q5);
          ls*cos(q1) + lt*cos(q2) + rtorso*cos(q5);
          -ls*sin(q1) - lt*sin(q2);
          ls*cos(q1) + lt*cos(q2);
          -ls*sin(q1) - lt*sin(q2) + (lt)*sin(q3);
          ls*cos(q1) + lt*cos(q2) - (lt)*cos(q3);
          -ls*sin(q1) - lt*sin(q2) + lt*sin(q3) + (ls)*sin(q4);
          ls*cos(q1) + lt*cos(q2) - lt*cos(q3) - (ls)*cos(q4)];
        
        jointsX = joints(1:2:end);
        jointsY = joints(2:2:end);
        
        plot(jointsX,jointsY,'linewidth',2,'marker','.','markersize',20,'color',[0,0,0]);%i/length(q1_sol)]);hold on;
        
        axis([-1,1,-.5,1.5]);axis equal;
        %   drawnow
        pause(.0001);
        
        
      end
    end
    
    % moving forward we'll suffix parameters from an optimal solution 'sol'
    function s = updateForOldNames(s)
      fNames = fieldnames(s);
      for i =1:length(fNames)
        tName = fNames{i};
        l = length(tName);
        if isequal(tName(l-4:end),'_plot')
          newName = [tName(1:(l-5)),'_sol'];
          s.(newName) = s.(tName);
          s = rmfield(s,tName);
        else
        end
        
      end
    end
    
    function s = allSols()
    b = whos();
    s = struct;
    for i =1:length(b)
      tStr = b(i).name;
      l = length(tStr)
      if length(tStr)>4
        if isequal(tStr(l-4:l),'_sol')
          eval('s.tStr = ',tStr);
        end
      end
    end
    end
    
    function [T,STATE,E,collisiondata,s]=simulateTomlabData(fname)
      %fname = 'positiveWorkCostNoCheat.mat';
      s = load(fname);
      s = WalkerTorso.updateForOldNames(s);
      s.q_sol = [s.q1_sol,s.q2_sol,s.q3_sol,s.q4_sol,s.q5_sol];
      x0 = [s.q1_sol(1);
        s.q2_sol(1);
        s.q3_sol(1);
        s.q4_sol(1);
        s.q5_sol(1);
        s.q1dot_sol(1);
        s.q2dot_sol(1);
        s.q3dot_sol(1);
        s.q4dot_sol(1);
        s.q5dot_sol(1);];
      
      P = struct;
      P.U = splineWrap(s.t_sol,[s.kneeTau1_sol,s.hipTau1_sol,s.hipTau2_sol,s.kneeTau2_sol]);
      
      [T,STATE] = ode45(@WalkerTorso.odeWalkerTorso,[s.t_sol(1):0.001:s.t_sol(end)],x0,[],P);
      statePre = STATE(end,:);
      if isfield(s,'Pushoff_sol')
        qdotPost = WalkerTorso.collision(statePre,s.Pushoff_sol);
      else
        fprintf('no pushoff in this solution');
        [qdotPost,eCols] = WalkerTorso.collision(statePre);
      end
      statePost =[ STATE(end,1:5),qdotPost(:)']';%column vector
      
      %%
      figure;
      plot(T,STATE(:,1:5),'linewidth',2);hold on;
      plot(s.t_sol,[s.q1_sol,s.q2_sol,s.q3_sol,s.q4_sol,s.q5_sol],'k');
      xlabel('Time (s)');
      ylabel('state \theta');
      
      E = WalkerTorso.energy(T,STATE,P);
      
      Epost = WalkerTorso.energy(0,statePost',P); % we need to rotate State since energy loops across dim1.
      EpostEk = Epost.Ek
      E.Ek(end) - EpostEk
      
      %%
      E.EMech(end)-E.EMech(1)
      E.workTau(end)-E.workTau(1)
      figure();
      plot(s.t_sol,E.powerAll);
      
      collisiondata.deltaEk = E.Ek(end)-EpostEk;
      collisiondata.netWork = E.workTau(end);
      collisiondata.dq = STATE(end,1:5)-STATE(1,1:5)
    end
    
  end
end