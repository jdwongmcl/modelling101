% stateT0=[0.353092546220743  -0.353092546220743  -0.502964015271127  -0.378750440270299];
% stateT0=[   0.353110000000000
%   -0.353110000000000];
stateT0=[ -0.198028407620313
  0.898028407620313
  0.198028407620313
  -0.898028407620313];

fname = 'walkerTorsoGivenstartingLocation';
vDesired = 0.4;
tEndGuess = 1.7;
nn = [10];
for iloop = 1:length(nn)
  if iloop==1
    tic
  end
  
  steps=nn(iloop);
  toms t tEnd argQ5
  p = tomPhase('p', t, 0, tEnd, steps, [], 'spline3');
  setPhase(p);
  
  % states of the system, matching kuo2001
  q1 = tomState('q1',1,1);
  q2 = tomState('q2',1,1);
  q3 = tomState('q3',1,1);
  q4 = tomState('q4',1,1);
  q5 = tomState('q5',1,1);
  
  q1dot = tomState('q1dot',1,1);
  q2dot = tomState('q2dot',1,1);
  q3dot = tomState('q3dot',1,1);
  q4dot = tomState('q4dot',1,1);
  q5dot = tomState('q5dot',1,1);
  
  PushoffOpt = tom('PushoffOpt',1,1); %PushoffOpt
  
  kneeTau1 = tomControl('kneeTau1',1,1);
  kneeTau2 = tomControl('kneeTau2',1,1);
  hipTau1 = tomControl('hipTau1',1,1);
  hipTau2 = tomControl('hipTau2',1,1);
  
  pospowers = tomState('pospowers',4,1);
  posfrates = tomState('posfrates',4,1);
  
  %%%% XXXX -> no initial guess.
  if iloop<2
    x0 = {tEnd == tEndGuess
      icollocate({
      q1 == stateT0(1)
      q2 == stateT0(2)
      q3 == stateT0(3)
      q4 == stateT0(4)
      q5 == 0;
      %       KpOpt == 0.0507
      })};
    %         collocate({
    %
    %         })};
  else
    x0 = {
      tEnd == tOpt
      icollocate({
      q1 == q1opt
      q2 == q2opt
      q3 == q3opt
      q4 == q4opt
      q5 == q5opt
      q1dot == q1dotopt
      q2dot == q2dotopt
      q3dot == q3dotopt
      q4dot == q4dotopt
      q5dot == q5dotopt
      })};
  end
  
  %% Boundary constraints
  %#jdw should change so that boundary constraints at the end are the same as
  %the begining, to keep cyclic.
  
  
  %%%
  gamma = 0;
  %   gamma = -5/180*pi;
  
  swingCostGainOopt = 0.04;
  g = 1;
  Mp = 0.68;
  Ms = .08;
  Mt = .08;
  
  %   L = 1;
  C = 0.25;
  %   Ip = 0;
  Il = 0.017004160000000;
  ls = 0.5;
  lt = 0.5;
  Is = Il/4;
  It = Il/4;
  Itorso = 0.0474;  % Dimensionless via Bobbert: b.sk.j(4)/sum(b.sk.mass)
  rtorso = 0.3;     % Bobbert: COM torso is 0.29 m
  
  
  % MM = zeros(2,2); rhs = zeros(2,1);
  % trying to debug this error:
  % The following error occurred converting from tomSym to double:
  % Cannot convert symbolic tomSym object to double.
  % does not like:
  % 1) a re-run where MM is being changed.
  % 2) if MM is defined as a double, and tomsym objects are part of the
  % equations.
  clear MassMat F G; % JDW thinks this is required. it may not be.
  
  MassMat = [   Is + C^2*Ms + Mp*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(q1 - q2)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),     -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*ls*cos(q1 - q4)*(C - ls), Mp*ls*rtorso*cos(q1 - q5);
    ls*cos(q1 - q2)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),      It + C^2*Mt + Mp*lt^2 + Ms*lt^2 + Mt*lt^2,     -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*lt*cos(q2 - q4)*(C - ls), Mp*lt*rtorso*cos(q2 - q5);
    -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),        -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt), It + C^2*Mt + Ms*lt^2 + Mt*lt^2 - 2*C*Mt*lt,      -Ms*lt*cos(q3 - q4)*(C - ls),                         0;
    Ms*ls*cos(q1 - q4)*(C - ls),                    Ms*lt*cos(q2 - q4)*(C - ls),                -Ms*lt*cos(q3 - q4)*(C - ls), Ms*C^2 - 2*Ms*C*ls + Ms*ls^2 + Is,                         0;
    Mp*ls*rtorso*cos(q1 - q5),                      Mp*lt*rtorso*cos(q2 - q5),                                           0,                                 0,      Mp*rtorso^2 + Itorso;];
  
  
  F =  [  Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2) - Ms*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + Ms*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mp*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mp*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2);
    Ms*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mp*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mp*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - C*Mt*sin(q2)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + C*Mt*cos(q2)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2);
    Ms*lt*sin(q3)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Ms*lt*cos(q3)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*sin(q3)*(C - lt)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) + Mt*cos(q3)*(C - lt)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2);
    Ms*cos(q4)*(C - ls)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*sin(q4)*(C - ls)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2);
    Mp*rtorso*cos(q5)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - Mp*rtorso*sin(q5)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2)];
  
  G = [ C*Ms*g*cos(gamma)*sin(q1) + C*Ms*g*cos(q1)*sin(gamma) + Mp*g*ls*cos(gamma)*sin(q1) + Mp*g*ls*cos(q1)*sin(gamma) + Ms*g*ls*cos(gamma)*sin(q1) + Ms*g*ls*cos(q1)*sin(gamma) + 2*Mt*g*ls*cos(gamma)*sin(q1) + 2*Mt*g*ls*cos(q1)*sin(gamma);
    C*Mt*g*cos(gamma)*sin(q2) + C*Mt*g*cos(q2)*sin(gamma) + Mp*g*lt*cos(gamma)*sin(q2) + Mp*g*lt*cos(q2)*sin(gamma) + Ms*g*lt*cos(gamma)*sin(q2) + Ms*g*lt*cos(q2)*sin(gamma) + Mt*g*lt*cos(gamma)*sin(q2) + Mt*g*lt*cos(q2)*sin(gamma);
    Mt*g*cos(gamma)*sin(q3)*(C - lt) - Ms*g*lt*cos(q3)*sin(gamma) - Ms*g*lt*cos(gamma)*sin(q3) + Mt*g*cos(q3)*sin(gamma)*(C - lt);
    Ms*g*cos(gamma)*sin(q4)*(C - ls) + Ms*g*cos(q4)*sin(gamma)*(C - ls);
    Mp*g*rtorso*cos(gamma)*sin(q5) + Mp*g*rtorso*cos(q5)*sin(gamma);];
  
  
  B =  [  1  0  0  0;
         -1  1  0  0;
          0  0  1 -1;
          0  0  0  1;
          0 -1 -1  0];
  
  taus = [ kneeTau1;
    hipTau1;
    hipTau2;
    kneeTau2];
  
  qddot = MassMat\ (F + G + B*taus);
  
  %% Equality EOM contraints
  conConstrainedEqEOM = collocate({
    dot([q1;q2;q3;q4;q5]) == [q1dot;q2dot;q3dot;q4dot;q5dot]
    dot([q1dot;q2dot;q3dot;q4dot;q5dot]) == qddot
    });
  
  
  %% movemend duration.
  %% XXXX
  % alternative is to specify symmetry and that the height of the foot is
  % zero at start
  % choose speed and time and step length is defined.
  phi1e = final(q1);
  phi2e = final(q2);
  phi3e = final(q3);
  phi4e = final(q4);
  
  
  phi10 = initial(q1);
  phi20 = initial(q2);
  phi30 = initial(q3);
  phi40 = initial(q4);
  
  swingHeightBeginning = ls*cos(phi10) + lt*cos(phi20) - lt*cos(phi30) - ls*cos(phi40);
  conConstrainedFootStart = { swingHeightBeginning == 0};
  
  
  %% collision
  
  %%%%%%%%%%%% system specific
  q1E = final(q1);
  q2E = final(q2);
  q3E = final(q3);
  q4E = final(q4);
  q5E = final(q5);
  q1dotE = final(q1dot);
  q2dotE = final(q2dot);
  q3dotE = final(q3dot);
  q4dotE = final(q4dot);
  q5dotE = final(q5dot);
  
  MassMatCol=[  Is + C^2*Ms + Mp*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(q1E - q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),   -ls*cos(q1E - q3E)*(Mp*lt + Ms*lt - C*Ms),     Mp*ls*cos(q1E - q4E)*(C - ls),      0, -cos(q1E)*(Mp*ls + Ms*ls + 2*Mt*ls + C*Ms), -sin(q1E)*(Mp*ls + Ms*ls + 2*Mt*ls + C*Ms);
    ls*cos(q1E - q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),        It + C^2*Mt + Mp*lt^2 + Ms*lt^2 + Mt*lt^2,   -lt*cos(q2E - q3E)*(Mp*lt + Ms*lt - C*Ms),     Mp*lt*cos(q2E - q4E)*(C - ls),      0,   -cos(q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),   -sin(q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt);
    -ls*cos(q1E - q3E)*(Mp*lt + Ms*lt - C*Ms),        -lt*cos(q2E - q3E)*(Mp*lt + Ms*lt - C*Ms), It + C^2*Ms + Mp*lt^2 + Ms*lt^2 - 2*C*Ms*lt,    -Mp*lt*cos(q3E - q4E)*(C - ls),      0,            cos(q3E)*(Mp*lt + Ms*lt - C*Ms),            sin(q3E)*(Mp*lt + Ms*lt - C*Ms);
    Mp*ls*cos(q1E - q4E)*(C - ls),                    Mp*lt*cos(q2E - q4E)*(C - ls),              -Mp*lt*cos(q3E - q4E)*(C - ls), Mp*C^2 - 2*Mp*C*ls + Mp*ls^2 + Is,      0,                      -Mp*cos(q4E)*(C - ls),                      -Mp*sin(q4E)*(C - ls);
    0,                                                0,                                           0,                                 0, Itorso,                                          0,                                          0;
    -cos(q1E)*(Mp*ls + Ms*ls + 2*Mt*ls + C*Ms),         -cos(q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),             cos(q3E)*(Mp*lt + Ms*lt - C*Ms),             -Mp*cos(q4E)*(C - ls),      0,                           Mp + 2*Ms + 2*Mt,                                          0;
    -sin(q1E)*(Mp*ls + Ms*ls + 2*Mt*ls + C*Ms),         -sin(q2E)*(Mp*lt + Ms*lt + Mt*lt + C*Mt),             sin(q3E)*(Mp*lt + Ms*lt - C*Ms),             -Mp*sin(q4E)*(C - ls),      0,                                          0,                           Mp + 2*Ms + 2*Mt;];
  
  %%% XXXX no impulse for now.
  % qdot includes x and y of the backfoot.
  %   qdot = [u1e;u2e;0;0];
  %   % note: x and y are both zero, because the back foot is on the ground.
  %   x =0;y=0;
  %   Jlambda_impulse = [0,0,1,0;
  %     0,0,0,1];
  %   angleCOM = atan2(-(L-R)*sin(q1e), R+(L-R)*cos(q1e)); %stolen from walk2uphill.
  %
  %   delta_q = MassMat \ Jlambda_impulse'* [sin(angleCOM);cos(angleCOM)]*POpt;
  %
  %   qdot = qdot+delta_q;
  %   uintr=qdot;
  %%%%%%%%%%%% original collision code.
  qdotE = [q1dotE;
    q2dotE;
    q3dotE;
    q4dotE
    q5dotE
    0;
    0;];
  Jlambda = [ -ls*cos(q1E), -lt*cos(q2E), lt*cos(q3E), ls*cos(q4E), 0, 1, 0;
    -ls*sin(q1E), -lt*sin(q2E), lt*sin(q3E), ls*sin(q4E), 0, 0, 1];
  
  
  
  
  epsilon = 0;%%%% /system specific but usual for collisions.
  %%%%%%%%%%%% /system specific
  
  %%%% generic
  cdot = Jlambda*qdotE;
  nc = length(cdot);
  
  MlambdaNI = MassMatCol \ Jlambda'; %'NI' is NotInverse; match Remy's convention.
  MlambdaNI = Jlambda * MlambdaNI;
  impulse = -1* MlambdaNI \ ((eye(nc,nc)+eye(nc,nc)*epsilon)*cdot);
  qdot_new = qdotE + MassMatCol \ Jlambda' * impulse;
  %%%% /generic
  
  %%specific
  %   unew = qdot_new;%since we do not want the x and y velocities to be returned
  %%%CONSTRAINTS
  %post collision. must be equal to u10 and u20.
  conConstrainedPostCollision = {
    final(q1) == initial(q4);
    final(q2) == initial(q3);
    final(q3) == stateT0(2);
    final(q4) == stateT0(1);
    final(q5) == argQ5;
    
    qdot_new(1) == initial(q4dot)
    qdot_new(2) == initial(q3dot)
    qdot_new(3) == initial(q2dot)
    qdot_new(4) == initial(q1dot)
    qdot_new(5) == initial(q5dot) %torso angular velocity has to be the same.
    };
  
  powKnee1 = kneeTau1.*(q1-q2);
  powHip1 = hipTau1.*(q2-q5);
  powHip2 = hipTau2.*(pi-q3-q5);
  powKnee2 = kneeTau2.*(q4+q3);
  objective_work = integrate(powKnee1 + abs(powKnee1) + ...
    powKnee2 + abs(powKnee2)+ ...
    powHip1 + abs(powHip1) + ...
    powHip2 + abs(powHip2));
  
  powers = [powKnee1;powHip1;powHip2;powKnee2];
  
  %%%CONSTRAINTS
  conPosPower = collocate({pospowers>=powers;
      pospowers>=0});

  coef = 8.5e-2;
  nnshift = 0.000001;
  
  %% jer needs to MODIFY HERE JDW
  objective_frate = coef*sum(integrate(sqrt(dot(dot(kneeTau1)).^2 + nnshift)) + ...
    + integrate(sqrt(dot(dot(kneeTau2)).^2 + nnshift)) + ...
    + integrate(sqrt(dot(dot(hipTau1)).^2 + nnshift))+ ...
    + integrate(sqrt(dot(dot(hipTau2)).^2 + nnshift)));
  
  frates = [dot(dot(kneeTau1));
        dot(dot(kneeTau2));
        dot(dot(hipTau1));
        dot(dot(hipTau2));];
  %%%CONSTRAINTS
  conPosFR = collocate({posfrates>=0
      posfrates >= frates});
  

    cbnd = {
    initial(q1) == stateT0(1);
    initial(q2) == stateT0(2);
    initial(q3) == stateT0(3);
    initial(q4) == stateT0(4);
    initial(q5) == argQ5;
    final(q5) == argQ5;
    %     final(q1) == initial(q1);
    %     initial(q1dot) == q1dot0;
    %     initial(q2dot) == q2dot0;
    %     initial(q3dot) == q3dot0;
    %     initial(q4dot) == q4dot0;
    %     -final(2) <= 0
    };
  
  
  
  %     cbnd ={};
  %%%CONSTRAINTS
  %% Box constraints
  % may want to prevent knee hyperextensions.
  cboxCheating  = { ...
  -pi/2 <= collocate(q5) <= pi/2
    %     -1<=initial(q1dot) <=-.1
    %     -1<=initial(q2dot) <=-.1
    %     -1<=initial(q3dot) <=-.1
    %     -1<=initial(q4dot) <=-.1
    %      -1<=initial(q4dot) <=-.1
    %       0<=initial(q3)<=pi/2
    %       0<=initial(q4)<=pi/2
    };
  %%%CONSTRAINTS

  sl = -ls*sin(phi1e) - lt*sin(phi2e) + lt*sin(phi3e) + ls*sin(phi4e);
    conConstrainedSpeed = { sl/tEnd == vDesired};

  
  objective = objective_work + objective_frate;
  %% Solve the problem
  options = struct;
  options.name = ['SNOPT walking model: collision with torso.'];
  OPTIONS.PriLev = 2;
  options.Prob.SOL.optPar(35) = 100000;
  options.Prob.SOL.optPar(30) = 200000;
  Prob = sym2prob(objective, ...
    {cbnd, cboxCheating, conConstrainedEqEOM, conConstrainedPostCollision, conConstrainedSpeed, conPosPower, conPosFR},...
    x0, options);
  result = tomRun('snopt',Prob,1);
  solution = getSolution(result);
  
  solutions(iloop)=solution;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  
  tOpt = subs(tEnd,solution);
  
  q1opt = subs(q1, solution);
  q2opt = subs(q2, solution);
  q3opt = subs(q3, solution);
  q4opt = subs(q4, solution);
  q5opt = subs(q5, solution);
  
  q1dotopt = subs(q1dot, solution);
  q2dotopt = subs(q2dot, solution);
  q3dotopt = subs(q3dot, solution);
  q4dotopt = subs(q4dot, solution);
  q5dotopt = subs(q5dot, solution);
  
  %   KpOut = subs(KpOpt,solution);
  %   KpPhi0Out = subs(KpPhi0Opt,solution);
  POut = subs(PushoffOpt,solution);
  kneeTau1Opt = subs(kneeTau1,solution);
  kneeTau2Opt = subs(kneeTau2,solution);
  hipTau1Opt = subs(hipTau1,solution);
  hipTau2Opt = subs(hipTau2,solution);
  
  t_plot = 0.001:0.001:tOpt; % Time vector where we want our trajectory evaluated.
  q1_plot = atPoints(t_plot,subs(q1,solution));
  q2_plot = atPoints(t_plot,subs(q2,solution));
  q3_plot = atPoints(t_plot,subs(q3,solution));
  q4_plot = atPoints(t_plot,subs(q4,solution));
  q5_plot = atPoints(t_plot,subs(q5,solution));
  kneeTau1_plot = atPoints(t_plot,subs(kneeTau1,solution));
  kneeTau2_plot = atPoints(t_plot,subs(kneeTau2,solution));
  hipTau1_plot = atPoints(t_plot,subs(hipTau1,solution));
  hipTau2_plot = atPoints(t_plot,subs(hipTau2,solution));
  
  
  %   u1_plot = atPoints(t_plot,subs(u1,solution));
  %   u2_plot = atPoints(t_plot,subs(u2,solution));
  plot(t_plot,[q1_plot,q2_plot,q3_plot,q4_plot,q5_plot]);
end

eval(['save ',fname,' solutions t_plot q1_plot q2_plot q3_plot q4_plot q5_plot']);
%% animate

figure;
plot(t_plot,[kneeTau1_plot,hipTau1_plot,hipTau2_plot,kneeTau2_plot]);