function outStruct = SnoptWalkerTorsoPushoffInvert(stateT0,fname,vDesired,nn,continuumFRate)

%%% convention: for each joint is relative to vertical.
% here are some demo stateT0 that don't return complete garbage, without
% any smart initial guess. 2020-04-08
% stateT0=[ -0.198028407620313
%   0.898028407620313
%   0.198028407620313
%   -0.898028407620313];
% 
% % almost straight knees
% stateT0=[0.238985083911023
%   0.461014916088977
%   -0.238985083911024
%   -0.461014916088976];
%%% /convention

tEndGuess = 1.7;

tic
for iloop = 1:length(nn)
  if iloop==1
    
  end
  
  steps=nn(iloop);
  toms t tEnd Pushoff %toms and tom do the same things, short decision var creation. 
  
  %   tEnd =     1.4634; tEndGuess = tEnd;
  p = tomPhase('p', t, 0, tEnd, steps, [], 'spline3');
  setPhase(p);
  
  %qdot
  qDot_Pre = tom('qDot_Pre',1,5);
  % states of the system, matching kuo2001
  q1 = tomState('q1',1,1);
  q2 = tomState('q2',1,1);
  q3 = tomState('q3',1,1);
  q4 = tomState('q4',1,1);
  q5 = tomState('q5',1,1);
  
  q1dot = tomState('q1dot',1,1);
  q2dot = tomState('q2dot',1,1);
  q3dot = tomState('q3dot',1,1);
  q4dot = tomState('q4dot',1,1);
  q5dot = tomState('q5dot',1,1);
  
  kneeTau1 = tomState('kneeTau1',1,1);
  kneeTau2 = tomState('kneeTau2',1,1);
  hipTau1 = tomState('hipTau1',1,1);
  hipTau2 = tomState('hipTau2',1,1);
  
  dotkneeTau1 = tomState('dotkneeTau1',1,1);
  dotkneeTau2 = tomState('dotkneeTau2',1,1);
  dothipTau1 = tomState('dothipTau1',1,1);
  dothipTau2 = tomState('dothipTau2',1,1);
  
  dotdotkneeTau1 = tomState('dotdotkneeTau1',1,1);
  dotdotkneeTau2 = tomState('dotdotkneeTau2',1,1);
  dotdothipTau1 = tomState('dotdothipTau1',1,1);
  dotdothipTau2 = tomState('dotdothipTau2',1,1);
  
  
  %%%% XXXX -> no initial guess.
  if iloop<2
    x0 = {tEnd == tEndGuess
      Pushoff == 0.1
      icollocate({
      q1 == stateT0(1)
      q2 == stateT0(2)
      q3 == stateT0(3)
      q4 == stateT0(4)
      q5 == 0;
      })};

  else
    x0 = {
      tEnd == tOpt
      Pushoff == pushoffOpt
      icollocate({
      q1 == q1opt
      q2 == q2opt
      q3 == q3opt
      q4 == q4opt
      q5 == q5opt
      q1dot == q1dotopt
      q2dot == q2dotopt
      q3dot == q3dotopt
      q4dot == q4dotopt
      q5dot == q5dotopt
      })};
  end
  
  %% Boundary constraints
  %#jdw should change so that boundary constraints at the end are the same as
  %the begining, to keep cyclic.
  
  cInitValues = {
    initial(q1) == stateT0(1);
    initial(q2) == stateT0(2);
    initial(q3) == stateT0(3);
    initial(q4) == stateT0(4);
    };
  
  %% Contraint: Equations of motion.
  gamma = 0; % decline. i.e., '-' is incline.
  %   gamma = -5/180*pi;
  g = 1;
  Mtorso = 0.68;
  Ms = .08;
  Mt = .08;
  
  %   L = 1;
  C = 0.25;
  %   Ip = 0;
  Il = 0.017004160000000;
  ls = 0.5;
  lt = 0.5;
  Is = Il/4;
  It = Il/4;
  Itorso = 0.0474;  % Dimensionless via Bobbert: b.sk.j(4)/sum(b.sk.mass)
  rtorso = 0.3;     % Bobbert: COM torso is 0.29 m
  
  
  %   clear MassMat F G; % JDW thinks this is required. it may not be tested 2020-02-19.
  MassMat = [   Is + C^2*Ms + Mtorso*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(q1 - q2)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),     -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*ls*cos(q1 - q4)*(C - ls), Mtorso*ls*rtorso*cos(q1 - q5);
    ls*cos(q1 - q2)*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),      It + C^2*Mt + Mtorso*lt^2 + Ms*lt^2 + Mt*lt^2,     -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt),       Ms*lt*cos(q2 - q4)*(C - ls), Mtorso*lt*rtorso*cos(q2 - q5);
    -ls*cos(q1 - q3)*(Ms*lt + Mt*lt - C*Mt),        -lt*cos(q2 - q3)*(Ms*lt + Mt*lt - C*Mt), It + C^2*Mt + Ms*lt^2 + Mt*lt^2 - 2*C*Mt*lt,      -Ms*lt*cos(q3 - q4)*(C - ls),                         0;
    Ms*ls*cos(q1 - q4)*(C - ls),                    Ms*lt*cos(q2 - q4)*(C - ls),                -Ms*lt*cos(q3 - q4)*(C - ls), Ms*C^2 - 2*Ms*C*ls + Ms*ls^2 + Is,                         0;
    Mtorso*ls*rtorso*cos(q1 - q5),                      Mtorso*lt*rtorso*cos(q2 - q5),                                           0,                                 0,      Mtorso*rtorso^2 + Itorso;];
  
  
  F =  [  Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2) - Ms*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + Ms*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mtorso*ls*sin(q1)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mtorso*ls*cos(q1)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2);
    Ms*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Mt*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) - Mtorso*lt*sin(q2)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2) + Mt*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2) + Mtorso*lt*cos(q2)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - C*Mt*sin(q2)*(ls*cos(q1)*q1dot^2 + C*cos(q2)*q2dot^2) + C*Mt*cos(q2)*(ls*sin(q1)*q1dot^2 + C*sin(q2)*q2dot^2);
    Ms*lt*sin(q3)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2) - Ms*lt*cos(q3)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Mt*sin(q3)*(C - lt)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + cos(q3)*(C - lt)*q3dot^2) + Mt*cos(q3)*(C - lt)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + sin(q3)*(C - lt)*q3dot^2);
    Ms*cos(q4)*(C - ls)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 - lt*sin(q3)*q3dot^2 + sin(q4)*(C - ls)*q4dot^2) - Ms*sin(q4)*(C - ls)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 - lt*cos(q3)*q3dot^2 + cos(q4)*(C - ls)*q4dot^2);
    Mtorso*rtorso*cos(q5)*(ls*sin(q1)*q1dot^2 + lt*sin(q2)*q2dot^2 + rtorso*sin(q5)*q5dot^2) - Mtorso*rtorso*sin(q5)*(ls*cos(q1)*q1dot^2 + lt*cos(q2)*q2dot^2 + rtorso*cos(q5)*q5dot^2)];
  
  G = [ C*Ms*g*cos(gamma)*sin(q1) + C*Ms*g*cos(q1)*sin(gamma) + Mtorso*g*ls*cos(gamma)*sin(q1) + Mtorso*g*ls*cos(q1)*sin(gamma) + Ms*g*ls*cos(gamma)*sin(q1) + Ms*g*ls*cos(q1)*sin(gamma) + 2*Mt*g*ls*cos(gamma)*sin(q1) + 2*Mt*g*ls*cos(q1)*sin(gamma);
    C*Mt*g*cos(gamma)*sin(q2) + C*Mt*g*cos(q2)*sin(gamma) + Mtorso*g*lt*cos(gamma)*sin(q2) + Mtorso*g*lt*cos(q2)*sin(gamma) + Ms*g*lt*cos(gamma)*sin(q2) + Ms*g*lt*cos(q2)*sin(gamma) + Mt*g*lt*cos(gamma)*sin(q2) + Mt*g*lt*cos(q2)*sin(gamma);
    Mt*g*cos(gamma)*sin(q3)*(C - lt) - Ms*g*lt*cos(q3)*sin(gamma) - Ms*g*lt*cos(gamma)*sin(q3) + Mt*g*cos(q3)*sin(gamma)*(C - lt);
    Ms*g*cos(gamma)*sin(q4)*(C - ls) + Ms*g*cos(q4)*sin(gamma)*(C - ls);
    Mtorso*g*rtorso*cos(gamma)*sin(q5) + Mtorso*g*rtorso*cos(q5)*sin(gamma);];
  
  
  % note about torques:
  % since the joints are knee1 hip1 hip2 knee2 torso,
  B =  [  -1  0  0  0;
    1 -1  0  0;
    0  0 -1  1;
    0  0  0 -1;
    0  1  1  0];
  
  taus = [ kneeTau1;
    hipTau1;
    hipTau2;
    kneeTau2];
  
  conEqEOM = collocate({
    dot([q1;q2;q3;q4;q5]) == [q1dot;q2dot;q3dot;q4dot;q5dot]
    (MassMat * dot([q1dot;q2dot;q3dot;q4dot;q5dot])) == (F + G + B*taus)
    dot([kneeTau1;hipTau1;hipTau2;kneeTau2;]) == [dotkneeTau1;dothipTau1;dothipTau2;dotkneeTau2;];
    dot([dotkneeTau1;dothipTau1;dothipTau2;dotkneeTau2;]) == [dotdotkneeTau1;dotdothipTau1;dotdothipTau2;dotdotkneeTau2;];
    });
  
  %% Constraint: desired speed.  
  qEndSim = [final(q1);final(q2);final(q3);final(q4);final(q5)];
  sl = -ls*sin(qEndSim(1)) - lt*sin(qEndSim(2)) + lt*sin(qEndSim(3)) + ls*sin(qEndSim(4));
  conAvgSpeed = { sl/tEnd == vDesired
    };
  
  %% collision equations.
  %%%%%%%%%%%% system specific
 
  %the legs start flipped
  qPre = [stateT0(4);
          stateT0(3);
          stateT0(2);
          stateT0(1);
          initial(q5)];
   
  MassMatCol=[...
    Is + C^2*Ms + Mtorso*ls^2 + Ms*ls^2 + 2*Mt*ls^2, ls*cos(qPre(1) - qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),   -ls*cos(qPre(1) - qPre(3))*(Ms*lt + Mt*lt - C*Mt),     Ms*ls*cos(qPre(1) - qPre(4))*(C - ls), Mtorso*ls*rtorso*cos(qPre(1) - qPre(5)), -cos(qPre(1))*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms), -sin(qPre(1))*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms);
    ls*cos(qPre(1) - qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),        It + C^2*Mt + Mtorso*lt^2 + Ms*lt^2 + Mt*lt^2,   -lt*cos(qPre(2) - qPre(3))*(Ms*lt + Mt*lt - C*Mt),     Ms*lt*cos(qPre(2) - qPre(4))*(C - ls), Mtorso*lt*rtorso*cos(qPre(2) - qPre(5)),   -cos(qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),   -sin(qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt);
    -ls*cos(qPre(1) - qPre(3))*(Ms*lt + Mt*lt - C*Mt),        -lt*cos(qPre(2) - qPre(3))*(Ms*lt + Mt*lt - C*Mt), It + C^2*Mt + Ms*lt^2 + Mt*lt^2 - 2*C*Mt*lt,    -Ms*lt*cos(qPre(3) - qPre(4))*(C - ls),                           0,            cos(qPre(3))*(Ms*lt + Mt*lt - C*Mt),            sin(qPre(3))*(Ms*lt + Mt*lt - C*Mt);
    Ms*ls*cos(qPre(1) - qPre(4))*(C - ls),                    Ms*lt*cos(qPre(2) - qPre(4))*(C - ls),              -Ms*lt*cos(qPre(3) - qPre(4))*(C - ls), Ms*C^2 - 2*Ms*C*ls + Ms*ls^2 + Is,                           0,                      -Ms*cos(qPre(4))*(C - ls),                      -Ms*sin(qPre(4))*(C - ls);
    Mtorso*ls*rtorso*cos(qPre(1) - qPre(5)),                      Mtorso*lt*rtorso*cos(qPre(2) - qPre(5)),                                           0,                                 0,        Mtorso*rtorso^2 + Itorso,                        -Mtorso*rtorso*cos(qPre(5)),                        -Mtorso*rtorso*sin(qPre(5));
    -cos(qPre(1))*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms),         -cos(qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),             cos(qPre(3))*(Ms*lt + Mt*lt - C*Mt),             -Ms*cos(qPre(4))*(C - ls),         -Mtorso*rtorso*cos(qPre(5)),                           Mtorso + 2*Ms + 2*Mt,                                          0;
    -sin(qPre(1))*(Mtorso*ls + Ms*ls + 2*Mt*ls + C*Ms),         -sin(qPre(2))*(Mtorso*lt + Ms*lt + Mt*lt + C*Mt),             sin(qPre(3))*(Ms*lt + Mt*lt - C*Mt),             -Ms*sin(qPre(4))*(C - ls),         -Mtorso*rtorso*sin(qPre(5)),                                          0,                           Mtorso + 2*Ms + 2*Mt];
  
  %%% Pushoff transitions states from qdotPre to qdotInt
  Jlambda_impulse = [0,0,0,0,0,1,0;
    0,0,0,0,0,0,1];
  Xhip = -ls*sin(qPre(1)) - C*sin(qPre(2));
  Yhip = ls*cos(qPre(1)) + C*cos(qPre(2));
  angleHip = atan2(Yhip, Xhip); %through the hip.
  
  delta_q = MassMatCol \ Jlambda_impulse'* [sin(angleHip);cos(angleHip)]*Pushoff;
  
  % stateT0 assumes stance-leg-first. 
  % our pre-collision is at the end of the stance-leg-last. 
  % hence the flip below, in qdotCol_Pre
  qdotCol_Pre = [qDot_Pre(1);qDot_Pre(2);qDot_Pre(3);qDot_Pre(4);qDot_Pre(5);0;0];
  qDotCol_Int = qdotCol_Pre+delta_q;
  
  Jlambda = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), lt*cos(qPre(3)), ls*cos(qPre(4)), 0, 1, 0;
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), lt*sin(qPre(3)), ls*sin(qPre(4)), 0, 0, 1];
  epsilon = 0;%%%% /system specific but usual for collisions.
  %%%%%%%%%%%% /system specific
  
  %%%%%%%%%%%% generic
  cdot = Jlambda*qDotCol_Int;
  nc = length(cdot);
  
  MlambdaNI = MassMatCol \ Jlambda'; %'NI' is NotInverse; match Remy's convention.
  MlambdaNI = Jlambda * MlambdaNI;
  impulse = -1* MlambdaNI \ ((eye(nc,nc)+eye(nc,nc)*epsilon)*cdot);
  %%% collision transitions to qdotPost
  qdotCol_Post = qDotCol_Int + MassMatCol \ Jlambda' * impulse;
  %%%%%%%%%%%% /generic
  
  %%specific
  %   unew = qdot_new;%since we do not want the x and y velocities to be returned
  
  %post collision. must be equal to u10 and u20.
    
  conCyclicPostCol = {
    % post-collision velocities are equal to the initial (leg swapped)
    qdotCol_Post(1) == initial(q4dot)
    qdotCol_Post(2) == initial(q3dot)
    qdotCol_Post(3) == initial(q2dot)
    qdotCol_Post(4) == initial(q1dot)
    qdotCol_Post(5) == initial(q5dot) %torso angular velocity has to be the same.
    
    % pre-collision velocities are equal to the end of simulation.
    qDot_Pre(1) == final(q1dot)
    qDot_Pre(2) == final(q2dot)
    qDot_Pre(3) == final(q3dot)
    qDot_Pre(4) == final(q4dot)
    qDot_Pre(5) == final(q5dot) 
    
    % symmetric positions.
    final(q1) == initial(q4) 
    final(q2) == initial(q3)
    final(q3) == initial(q2)
    final(q4) == initial(q1)
    final(q5) == initial(q5)};
  
  %% energy for the entire movement.
  qdot = [q1dot;q2dot;q3dot;q4dot;q5dot];
  Jek1 = [-C*cos(q1), 0, 0, 0, 0;
    -C*sin(q1), 0, 0, 0, 0];
  
  Jek2 = [ -ls*cos(q1), -C*cos(q2), 0, 0, 0;
    -ls*sin(q1), -C*sin(q2), 0, 0, 0];
  
  Jek3 = [ -ls*cos(q1), -lt*cos(q2), -cos(q3)*(C - lt), 0, 0;
    -ls*sin(q1), -lt*sin(q2), -sin(q3)*(C - lt), 0, 0];
  
  Jek4 = [ -ls*cos(q1), -lt*cos(q2), lt*cos(q3), -cos(q4)*(C - ls), 0;
    -ls*sin(q1), -lt*sin(q2), lt*sin(q3), -sin(q4)*(C - ls), 0];
  
  Jek5 = [ -ls*cos(q1), -lt*cos(q2), 0, 0, -rtorso*cos(q5);
    -ls*sin(q1), -lt*sin(q2), 0, 0, -rtorso*sin(q5)];
  
  Masses = [Ms;Mt;Mt;Ms;Mtorso];
  Ekl = 1/2*...
    (Masses(1)*(Jek1*qdot)' * (Jek1*qdot) + ...
    Masses(2)*(Jek2*qdot)' * (Jek2*qdot) + ...
    Masses(3)*(Jek3*qdot)' * (Jek3*qdot) + ...
    Masses(4)*(Jek4*qdot)' * (Jek4*qdot) + ...
    Masses(5)*(Jek5*qdot)' * (Jek5*qdot));
  
  Irots = [Is;It;It;Is;Itorso];
  Ekr = 1/2 * ...
    (Irots(1)*qdot(1)^2+...
    Irots(2)*qdot(2)^2+...
    Irots(3)*qdot(3)^2+...
    Irots(4)*qdot(4)^2+...
    Irots(5)*qdot(5)^2);
  
  Eg= Masses(1) * g * C*cos(q1) + ...
    Masses(2) * g * (ls*cos(q1) + C*cos(q2)) + ...
    Masses(3) * g * (ls*cos(q1) + lt*cos(q2) - (lt-C)*cos(q3)) + ...
    Masses(4) * g * (ls*cos(q1) + lt*cos(q2) - lt*cos(q3) - (ls-C)*cos(q4)) + ...
    Masses(5) * g * (ls*cos(q1) + lt*cos(q2) + rtorso*cos(q5));
  
  Ek = Ekl+Ekr;
  EMech = Ekl+Ekr+Eg;
  EgDelta = final(Eg)-initial(Eg);
  
  EkPre = final(Ek);
  EMechPre = final(Ek)+final(Eg);
  %% kinetic energy of INTER
  
  Jek1E = [-C*cos(qPre(1)), 0, 0, 0, 0;
    -C*sin(qPre(1)), 0, 0, 0, 0];
  
  Jek2E = [ -ls*cos(qPre(1)), -C*cos(qPre(2)), 0, 0, 0;
    -ls*sin(qPre(1)), -C*sin(qPre(2)), 0, 0, 0];
  
  Jek3E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), -cos(qPre(3))*(C - lt), 0, 0;
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), -sin(qPre(3))*(C - lt), 0, 0];
  
  Jek4E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), lt*cos(qPre(3)), -cos(qPre(4))*(C - ls), 0;
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), lt*sin(qPre(3)), -sin(qPre(4))*(C - ls), 0];
  
  Jek5E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), 0, 0, -rtorso*cos(qPre(5));
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), 0, 0, -rtorso*sin(qPre(5))];
  
  Masses = [Ms;Mt;Mt;Ms;Mtorso];
  
  qdot_inter1to5 = qDotCol_Int(1:5);
  EkLinInter = 1/2* ...
    (Masses(1)*(Jek1E*qdot_inter1to5)' * (Jek1E*qdot_inter1to5) + ...
    Masses(2)*(Jek2E*qdot_inter1to5)' * (Jek2E*qdot_inter1to5) + ...
    Masses(3)*(Jek3E*qdot_inter1to5)' * (Jek3E*qdot_inter1to5) + ...
    Masses(4)*(Jek4E*qdot_inter1to5)' * (Jek4E*qdot_inter1to5) + ...
    Masses(5)*(Jek5E*qdot_inter1to5)' * (Jek5E*qdot_inter1to5));
  
  Irots = [Is;It;It;Is;Itorso];
  EkRotInter = 1/2 * sum((Irots .* qdot_inter1to5.^2));
  
  EkInter = EkLinInter + EkRotInter;
  EMechInter = EkInter+final(Eg);
  %% kinetic energy of POST collision
  
  Jek1E = [-C*cos(qPre(1)), 0, 0, 0, 0;
    -C*sin(qPre(1)), 0, 0, 0, 0];
  
  Jek2E = [ -ls*cos(qPre(1)), -C*cos(qPre(2)), 0, 0, 0;
    -ls*sin(qPre(1)), -C*sin(qPre(2)), 0, 0, 0];
  
  Jek3E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), -cos(qPre(3))*(C - lt), 0, 0;
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), -sin(qPre(3))*(C - lt), 0, 0];
  
  Jek4E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), lt*cos(qPre(3)), -cos(qPre(4))*(C - ls), 0;
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), lt*sin(qPre(3)), -sin(qPre(4))*(C - ls), 0];
  
  Jek5E = [ -ls*cos(qPre(1)), -lt*cos(qPre(2)), 0, 0, -rtorso*cos(qPre(5));
    -ls*sin(qPre(1)), -lt*sin(qPre(2)), 0, 0, -rtorso*sin(qPre(5))];
  
  Masses = [Ms;Mt;Mt;Ms;Mtorso];
  
  qdot_post1to5 = qdotCol_Post(1:5);
  EkLinPost = 1/2* ...
    (Masses(1)*(Jek1E*qdot_post1to5)' * (Jek1E*qdot_post1to5) + ...
    Masses(2)*(Jek2E*qdot_post1to5)' * (Jek2E*qdot_post1to5) + ...
    Masses(3)*(Jek3E*qdot_post1to5)' * (Jek3E*qdot_post1to5) + ...
    Masses(4)*(Jek4E*qdot_post1to5)' * (Jek4E*qdot_post1to5) + ...
    Masses(5)*(Jek5E*qdot_post1to5)' * (Jek5E*qdot_post1to5));
  
  Irots = [Is;It;It;Is;Itorso];
  EkRotPost = 1/2 * sum((Irots .* qdot_post1to5.^2));
  
  EkPost = EkLinPost + EkRotPost;
  EMechPost = EkPost+final(Eg);
  %% Objectives
  
  % powers...which i know to be correct...
  powKnee1 = kneeTau1.*(q2dot-q1dot);% positive thigh , negative shank
  powHip1 =   hipTau1.*(q5dot-q2dot);% positive torso , negative thigh1
  powHip2 =   hipTau2.*(q5dot-q3dot);% positive torso , negative thigh2
  powKnee2 = kneeTau2.*(q3dot-q4dot);% positive thigh2, negative shank
  
  EJointWork = integrate(powKnee1+powKnee2+powHip1+powHip2);
  conEnergy = {final(EJointWork) == atPoints(Ek,tEnd)-EkPost};
  
  EPositiveJointWork = integrate(...
    (powKnee1 + abs(powKnee1))/2 + ...
    (powKnee2 + abs(powKnee2))/2 + ...
    (powHip1 + abs(powHip1))/2 + ...
    (powHip2 + abs(powHip2))/2 );
  
  EPushoffWork = EMechInter-EMechPre;
  ECollision = EkPost - EkInter;
  objective_positive_work = EPositiveJointWork + EPushoffWork;
  
  conPushoffPositive = {EPushoffWork >= 0};
  
  coef = 3e-4;
  objective_frate = coef*(sum(integrate(dotdotkneeTau1.^2) + ...
    integrate(dotdothipTau1.^2) + ...
    integrate(dotdothipTau2.^2) + ...
    integrate(dotdotkneeTau2.^2)));
  
  objective = continuumFRate(iloop) * (objective_positive_work+objective_frate);
  %   objective = 1;
  %% Solve the problem
  options = struct;
  options.name = ['SNOPT walking model: collision with torso. implicit formulation of EOM.'];
  OPTIONS.PriLev = 2;
  options.Prob.SOL.optPar(35) = 100000;
  options.Prob.SOL.optPar(30) = 200000;
  Prob = sym2prob(objective, ...
    {cInitValues, conCyclicPostCol, conEqEOM, conPushoffPositive, conAvgSpeed},...
    x0, options);
  result = tomRun('snopt',Prob,1);
  solution = getSolution(result);
  
  solutions(iloop)=solution;
  f_k=result.f_k;
  Exits(iloop,:)=[result.ExitFlag result.Inform];
  
  tOpt = subs(tEnd,solution);
  
  q1opt = subs(q1, solution);
  q2opt = subs(q2, solution);
  q3opt = subs(q3, solution);
  q4opt = subs(q4, solution);
  q5opt = subs(q5, solution);
  
  q1dotopt = subs(q1dot, solution);
  q2dotopt = subs(q2dot, solution);
  q3dotopt = subs(q3dot, solution);
  q4dotopt = subs(q4dot, solution);
  q5dotopt = subs(q5dot, solution);
  pushoffOpt = subs(Pushoff,solution);
  
  kneeTau1Opt = subs(kneeTau1,solution);
  kneeTau2Opt = subs(kneeTau2,solution);
  hipTau1Opt = subs(hipTau1,solution);
  hipTau2Opt = subs(hipTau2,solution);
  
  t_sol = 0.001:0.001:tOpt; % Time vector where we want our trajectory evaluated.
  q1_sol = atPoints(t_sol,subs(q1,solution));
  q2_sol = atPoints(t_sol,subs(q2,solution));
  q3_sol = atPoints(t_sol,subs(q3,solution));
  q4_sol = atPoints(t_sol,subs(q4,solution));
  q5_sol = atPoints(t_sol,subs(q5,solution));
  
  q1dot_sol = atPoints(t_sol,subs(q1dot,solution));
  q2dot_sol = atPoints(t_sol,subs(q2dot,solution));
  q3dot_sol = atPoints(t_sol,subs(q3dot,solution));
  q4dot_sol = atPoints(t_sol,subs(q4dot,solution));
  q5dot_sol = atPoints(t_sol,subs(q5dot,solution));
  
  kneeTau1_sol = atPoints(t_sol,subs(kneeTau1,solution));
  kneeTau2_sol = atPoints(t_sol,subs(kneeTau2,solution));
  hipTau1_sol = atPoints(t_sol,subs(hipTau1,solution));
  hipTau2_sol = atPoints(t_sol,subs(hipTau2,solution));
  
  powk1 = atPoints(t_sol,subs(powKnee1,solution));
  powh1 = atPoints(t_sol,subs(powHip1,solution));
  powk2 = atPoints(t_sol,subs(powKnee2,solution));
  powh2 = atPoints(t_sol,subs(powHip2,solution));
  
  objective_sol = subs(objective,solution);
  objective_frate_sol = subs(objective_frate, solution);
  
  plot(t_sol,[q1_sol,q2_sol,q3_sol,q4_sol,q5_sol]);
  figure;
  plot(t_sol,[powk1,powh1,powk2,powh2]);
  legend('powk1','powh1','powk2','powh2');
  
  
  EMech_sol = atPoints(t_sol,subs(EMech,solution));
  
  EkPre_sol = subs(EkPre,solution);
  EkInter_sol = subs(EkInter,solution);
  EkPost_sol = subs(EkPost,solution);
  ekVecPreCol_sol = atPoints(t_sol,subs(Ekl+Ekr,solution));
  EJointWork_sol = atPoints(t_sol,subs(EJointWork,solution));
  EPushoffWork_sol = subs(EPushoffWork,solution);
  angleHip_sol = subs(angleHip,solution);
  
  Pushoff_sol = subs(Pushoff, solution);
  
  EgDelta_sol = subs(EgDelta,solution);
  Etot_sol = atPoints(t_sol,subs(EMech,solution));
  ECollision_sol = EkPost_sol - EkInter_sol;
  fprintf('collision work (should be negative): %.3f\n',ECollision_sol);
  fprintf('angle at pushoff: %.3f\n',angleHip_sol);
  ENetJointWork_sol = EJointWork_sol(end);
  EError_sol = EPushoffWork_sol + ENetJointWork_sol + ECollision_sol+EgDelta_sol;
  FractionalEError_sol = EError_sol/EMech_sol(1);
  fprintf('Round %i of %i: NODES: %i \n',iloop,length(nn),nn(iloop));
  fprintf('---------------\n');
  fprintf('Net joint work: %.4f\n',ENetJointWork_sol);
  fprintf('Pushoff work: %.4f\n',EPushoffWork_sol);
  fprintf('Total work: %.4f\n',EPushoffWork_sol+ENetJointWork_sol);
  fprintf('Collision work (should be negative): %.4f\n',ECollision_sol);
  fprintf('Work error: %.4f\n',EPushoffWork_sol + ENetJointWork_sol + ECollision_sol);
  fprintf('Energy error:%.4f\n',EError_sol);
  fprintf('Fractional Energy error:%.4f\n',FractionalEError_sol);
  fprintf('Objective value: %.4f\n',objective_sol);
  fprintf('frate: %.4f\n',objective_frate_sol);
  figure;
  plot(t_sol,[EJointWork_sol,Etot_sol]);
end
totalOptTime=toc;
fprintf('total opt time: for %i loops: %.1f\n',length(nn),totalOptTime);
eval(['save ',fname,' totalOptTime Exits statePostFlip_sol Pushoff_sol Etot_sol EgDelta_sol EJointWork_sol ENetJointWork_sol ECollision_sol EPushoffWork_sol solutions powk1 powk1 powk2 powh2 kneeTau1_sol kneeTau2_sol hipTau1_sol hipTau2_sol t_sol q1_sol q2_sol q3_sol q4_sol q5_sol q1dot_sol q2dot_sol q3dot_sol q4dot_sol q5dot_sol']);

% dump all things suffixed with 'sol' into an output struct. 
b = whos();
outStruct = struct;
for i =1:length(b)
  tStr = b(i).name;
  l = length(tStr);
  if length(tStr)>4
    if isequal(tStr(l-3:l),'_sol')
      eval(['s.',tStr,' = ',tStr,';']);
    end
  end
end

end%fn