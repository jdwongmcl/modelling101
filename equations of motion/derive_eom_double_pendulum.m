%% EOM for a 2df arm

clc; clear variables;
syms m0 m1 m2 I0 I1 l0 l1 g d0 d1 real
syms q0 q0dot q1 q1dot real
%% DEFINE
n_dim = 2;

% XY of the bodies.
XYS = [d0*cos(q0);
    d0*sin(q0);
    l0*cos(q0) + d1*cos(q1);
    l0*sin(q0) + d1*sin(q1);
    l0*cos(q0) + l1*cos(q1);
    l0*sin(q0) + l1*sin(q1)];

% external reference frame angle of the bodies
PHI_BODY_INERTIALFRAME = [q0;q1;q1]; % PHI_BODY_INERTIALFRAME: what is the orientation of the body in inertial reference frame?
m=[m0;m1;m2];
I=[I0;I1;0];
n_bodies = length(m);

% independent dimensions.
q = [q0;q1];
qdot=[q0dot;q1dot];

n_q = length(q);
%% THAT IS IT!!
%% this should be the same for all 2d systems.
%% dimensions of M F G corrected 2017-03-29
M = zeros(n_q,n_q);
F = zeros(n_q,1);
G = zeros(n_q,1);
for i =1:n_bodies
    XY_cur = XYS((i-1)*n_dim+1:(i)*n_dim,:);
    PHI_cur = PHI_BODY_INERTIALFRAME(i);
    JF = jacobian(XY_cur,q); 
    fprintf('Jacobian %i\n',i);
    disp(JF);
    JR = jacobian(PHI_cur,q);
    sigma = jacobian(JF*qdot, q)*qdot;
    sigma_R = jacobian(JR*qdot,q)*qdot;
    meye = eye(n_dim,n_dim)*m(i);
    M = M + JF' * meye * JF + ...
        JR' * I(i) * JR;
    M = simplify(expand(M));
    F = F - ( JF' * meye * sigma + JR' * I(i) * sigma_R); %minus is Remy convention.
    
    % rotation matix
    gam = 0;
    A_IB = [[cos(gam);sin(gam)],[-sin(gam);cos(gam)]];

    FG = m(i)*-g*A_IB'*[0;1];
    G = G + JF'*FG;
end
F=simplify(expand(F));
G=simplify(expand(G));
Minv = inv(M);
% qdotdot = M \ (G + F); %in general, must be careful with '\' order of operations.
% in general we don't pre-compute ddQ, because this is hard on matlab. even
% for quite small systems. Better to compute M, F, and G, and then solve
% for ddq numerically. 
