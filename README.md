# README #

This short course is meant to get undergrad students interested and familiar with computational modelling for motor control!

### What is this repository for? ###

* ode simulations using equations of motion
* energy balance
* using tomlab via the server.

### How do I get set up? ###

* Summary of set up
to run the tomlab demo, you'll need to use the tomlab server on campus. This means you'll need 'Microsoft Remote Desktop', as well as
Forticlient: https://iac01.ucalgary.ca/SDSWeb/Public/SoftwareTypeList.aspx
Microsoft Remote Desktop https://www.microsoft.com/en-us/p/microsoft-remote-desktop/9wzdncrfj3ps

IP for tomlab server: 136.159.130.104.

default password: check our slack!
on D:\ there is a link, 'tomlabMatlab'; same password to start matlab with tomlab installed!

download this repository, possibly using GIT. 

### Contribution guidelines ###


### Who do I talk to? ###

jeremy.wong2@ucalgary.ca