classdef fvpushsim < handle
  properties (Access = public)
    mass    = 0;
    g       = 9.81;
    fcoef   = 0;
    fvslope = 0;
    maxF    = 0;
    b       = 0;
  end
  methods

    function obj=fvpushsim(inmass,inG,inFCoef,fvslope,b)
%       arguments
%         inmass double{mustBePositive}     = 80;
%         inG double{mustBePositive}        = 9.81;
%         inFCoef double{mustBePositive}    = 0.2;
%         fvslope double{mustBeNumeric}     = 0;
%         b double{mustBeNumeric}           = 0;
%       end
      obj.mass    = inmass;
      obj.g       = inG;
      obj.fcoef   = inFCoef;
      obj.fvslope = fvslope;
      obj.maxF    = b;
    end

    function setFVFromTwoPoints(obj,point1,point2)
       obj.fvslope = (point2(2)-point1(2))./ point2(1)-point1(1);
      if obj.fvslope == inf
        obj.maxF = 0;
      else
        obj.maxF = point1(2) - obj.fvslope*point1(1);
      end
    end

    function [stated] = odeFV(obj, t, state) 
      b     = obj.maxF;
      slope = obj.fvslope;
      m     = obj.mass;
      fc    = obj.fcoef;
      grav  = obj.g;

      x     = state(1); %#ok<NASGU> 
      dxdt  = state(2);

      fpush = obj.pushForce(dxdt);
      ffric = obj.frictionForce(dxdt);
      fdrag = obj.dragForce(dxdt);
      sumF  = fpush - fdrag - ffric ;

      ddxdt2 = 1/m * (sumF);
      stated = [dxdt;ddxdt2];
    end
    
    % friction force model. 
    function ffric = frictionForce(obj, dxdt)
      fnorm = obj.g * obj.mass;  
      ffric = obj.fcoef * fnorm * 1./(1+exp(-(dxdt-.1)*100));
    end
    
    function fdrag = dragForce(obj, dxdt)
      fdrag = obj.b * dxdt.^2;
    end

    % pushing force model.
    function out = pushForce(obj,dxdt)
      out = dxdt*obj.fvslope + obj.maxF;
    end

  function out = energyBalance(obj,t,state)
    dxdt = state(:,2);
    fp    = obj.pushForce(dxdt);
    ff    = obj.frictionForce(dxdt);
    fdr   = obj.dragForce(dxdt);
    powp  = fp.*dxdt;
    powf  = ff.*dxdt;
    powd  = fdr.*dxdt;
    ek    = 1/2*obj.mass*dxdt.^2;
    
    workOn    = cumtrapz(t,powp);
    workSinks = cumtrapz(t,powf) + cumtrapz(t,powd) + ek;

    out.workOn    = workOn;
    out.workSinks = workSinks;
    out.ek        = ek;
    out.friction  = cumtrapz(t,powf);
    out.drag      = cumtrapz(t,powd);
  end

  end
  
  methods(Static)            % ------STATIC------ %
    
    function stated = wrapodeFV(t,state,model)
%       arguments
%         t double{mustBePositive}          = 80;
%         state double{mustBeVector}        = 9.81;
%         model fvpushsim                   = fvpushsim();
%       end
      stated = model.odeFV(t,state);
    end

    function demo()
      model     = fvpushsim(80,9.81,0.2,0,0);
      model.setFVFromTwoPoints([0,300],[7,0]);
      model.b = 20;
      tend      = 10; 
      icond     = [0,0];
      [t,state] = ode45(@fvpushsim.wrapodeFV,[0,tend],icond,[],model);
      fp = model.pushForce(state(:,2));
      ff = model.frictionForce(state(:,2));
      fd = model.dragForce(state(:,2));
      e  = model.energyBalance(t,state);
      % do some plotting
      figure();
      plot(t,state);
      xlabel('Time (s)');
      ylabel('position (m) and velocity (m/s)');
      legend({'position','velocity'});
      figure();
      plot(state(:,2),fp);
      hold on;
      plot(state(:,2),ff);
      plot(state(:,2),fd);
      ylim([0,model.maxF]);
      xlabel('velocity');
      ylabel('forces');
      legend({'push','friction','drag'});
      figure();
      plot(t,e.workOn,'linewidth',3);
      hold on;
      plot(t,e.workSinks,'--','linewidth',2);
      legend({'pushing work','ek + friction + drag'});

    end
    function demo3()
      fcoef = 0.1;
      b = 0;
      masses = 10:10:600;
      [v,f] = fvpushsim.fvtests(masses,100,12,fcoef,b);
      figure(20);hold on;
      plot(v,f.*v)
      [v,f] = fvpushsim.fvtests(masses,300,8,fcoef,b);
      figure(20);
      plot(v,f.*v)
      [v,f] = fvpushsim.fvtests(masses,600,5,fcoef,b);
      figure(20);
      plot(v,f.*v)

    end
    function demo2()
      % do a franzi pushtest where there is no dominant drag force. just friction.
      masses = 10:10:200;
      
      fcoef = 0.4;
      
      d1 = 0; d2 = 25;
      spd1 = 6;
      fmax = 300;
      fvpushsim.fvtests(masses,fmax,spd1,fcoef,d1);
      pause;
      fvpushsim.fvtests(masses,fmax,spd1,fcoef,d2);
      fprintf('note that the two simulations produce different points but are on the same line.\n');
      legend({sprintf('drag %i spd %i',d1,spd1),sprintf('drag %i, spd %i',d2,spd1)})
      pause;
      figure(24);close;
      spd2 = 8
      fvpushsim.fvtests(masses,fmax,spd2,fcoef,d1);
      fvpushsim.fvtests(masses,fmax,spd2,fcoef,d2);
      legend({sprintf('drag %i spd %i',d1,spd1),sprintf('drag %i, spd %i',d2,spd1), ...
        sprintf('drag %i spd %i',d1,spd2),sprintf('drag %i, spd %i',d2,spd2)})
      fprintf('and a subject with a different top speed and same max force has a different fv.\n')

    end

    function [v,f] = fvtests(masses,maxF,maxV,fcoef,b)
      model     = fvpushsim(80,9.81,0.2,0,0);
      model.setFVFromTwoPoints([0,maxF],[maxV,0]);
      model.b = b;
      model.fcoef = fcoef;
      tend      = 10; 
      icond     = [0,0];
      
      for imass = 1:length(masses)
        model.mass = masses(imass);
        figure(24);
        hold on;
        [t,state] = ode45(@fvpushsim.wrapodeFV,[0,tend],icond,[],model);
        plot(t,state(:,2));
        xlabel('time (s)');
        ylabel('speed (m/s)')
        v(imass)  = state(end,2);
        fpush = model.pushForce(state(:,2));
        f(imass)  = fpush(end);
      end
      figure(25);hold on;
      plot(v,f,'.');
      ylim([0,500]);
      xlim([0,15]);
      xlabel('velocity');
      ylabel('force');
    end
  end
end
