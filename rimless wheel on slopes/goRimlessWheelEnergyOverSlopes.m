fprintf('You will need to add $dynamicwalking/src to path.\n');
fprintf('note: do not use genpath($dynamicwalking/src).\n');

wu = walkrw2uneventimeopt();  % uneven terrain

clear inc cost e
inc = [-40:1:40]
% inc = [-3]
xstarNominal = get(wu,'xstar');
dxlast = xstarNominal(2);
for i =1:length(inc)

    %%% pull from the generic rimless wheel walking model the 1/2legangle,
    %%% and compute the step duration from fixed speed (dimless=0.4).
    alpha = get(wu,'alpha'); % leg angle
    d = 2*sin(alpha);        % set it
    v = 0.4;                 % dimensionless speed
    t = d/v;                 % fixed time for step at fixed speed/steplength
    xstar = get(wu,'xstar'); %xstar -> states the walker returns to

    %%% set fixed aspects of the sloped walk. the duration (steptime) and
    %%% slope (gamma)
    wu = walkrw2uneventimeopt();        % avoid looping bugs by new walker each time.
    wu = set(wu,'gamma',inc(i)/180*pi); % gamma is slope. NOTE! - is up.
    wu = set(wu,'steptime',t);          % set the step time.
    %wu = set(wu,'P',);

    %%% setup optimizer
    fOpt=1;                             % dummy optimization function: it turns out there is no choice.
    optopts = optimset('fmincon');      % not tomlab, fmincon()
    optopts.ConstraintTolerance=1e-10;  % ensure no violations.
    optopts.Algorithm='sqp';            % common solver
    guess=[xstar(2),get(wu,'P')];       % P is pushoff.

    % now optimize!
    [farg,fval,exit]=fmincon(@(x)optCost(x,wu),guess,[],[],[],[],[],[],@(x)nlconLimitCycle(x,wu),optopts);

    %%% save the optimization results in 'fargs', and the energies in 'e'.
    fargs(i,:) = farg(:); %save the optimal arguments (pushoff angle and speed)
    [ineq,eq]=nlconLimitCycle(farg,wu);
    wu = set(wu,'xstar',[xstar(1),farg(1)]);
    wu = set(wu,'P',farg(2));
    exits(i) = exit;
    [a,b,c,d,e(i)]=onesteptime(wu);
    dxlast = farg(1);

    %     pause;
end
%% Okay cool! we have now swept across angles and stored all of the
% energy information in 'e'.
%
for i =1:length(e)
    cost(i) = e(i).pushoffwork;
    slope(i) = -inc(i);%just flip it since i think of - as down...

    % assignment: compute these three terms! really just looking up the
    % 'energy' structure e, which has fields; and computing what the power
    % from gravity must be given the slope and walking speed.
    % when you know, set if to be 1

    %%% compute these
    powerCollision(i) = 000.000
    power(i) = 000.000
    powerGrav(i) = 000.000
    %%% compute these
end



