function results = optimizeSnoptKuo2001PFSweepKpPhi0GivenVdesSL(w,vDes,slDes,gamma,KpPhi0In,fPrefix)
% function results = optimizeSnoptKuo2001PushoffFirstGivenVdesSL(w,vDes,SLDes,gamma,fPrefix)
% 
tGuess = 1.7;
L = get(w,'L');
R = get(w,'R');
%convert the step-length to an angle. 
f_slError = @(theta,SLgoal,L,R) (2*((L-R)*sin(theta) + R*theta) - SLgoal).^2; %step length for symmetric step.
% find q1 i.e. theta that produces a given step length.
thetaGuess = slDes/(2*(L-R)+R); %linearized step.

 opts = optimoptions('fmincon');
 opts.Algorithm = 'sqp';
[alphaforSL,~,~]=fmincon(@(theta)f_slError(theta,slDes,L,R),thetaGuess,...
  [],[],[],[],0,1,[],opts);
slCheck = 2*((L-R)*sin(alphaforSL) + R*alphaforSL);
if abs(slCheck - slDes) > 1e-5
  error('did not find a q1 to solve for the desired step length.\n');
end
numNodes = 40;
for iloop = 1:length(numNodes)
  if iloop==1
    tic
  end
  
  %% tomlab parameters.
  steps=numNodes(iloop);
  % optimization arguments. 
  u1precollision= tom('u1precollision',1,1);
  u2precollision= tom('u2precollision',1,1);  
  Kp = tom('Kp',1,1);
  P = tom('P',1,1);
  
  toms t tEnd %honestly i'm not sure whyt he different syntax here. 
  % phase set the time. here you could have phases of different length. 
  p = tomPhase('p', t, 0, tEnd, steps, [], 'spline3'); %also set how we
  setPhase(p);
  
  % states of the system, matching kuo2001
  q1 = tomState('q1',1,1);
  q2 = tomState('q2',1,1);
  u1 = tomState('qdot1',1,1);
  u2 = tomState('qdot2',1,1);
  %% initial guesses for possibly time (ALWAYS FIRST), states, controls, and opt arguments. 
  % seems to be fine to leave some guesses blank.
  if iloop<2
    x0 = {
      tEnd == tGuess;
      icollocate({
      q1 == alphaforSL;
      q2 == -alphaforSL;
      P ==.12
      Kp == 0.05
      })};
    
  else
    x0 = {
      tEnd == tOpt;
      icollocate({
      q1 == q1opt
      q2 == q2opt
      u1 == u1opt
      u2 == u2opt    
      })};
  end

  %% parameters for dynamics and collision.
%   KpPhi0 = KpPhi0Opt;
%   Kp = Kp;
%   P = P;
%   
  KpPhi0 = KpPhi0In;
  g = get(w,'g');
  Mp = get(w,'Mp');
  M = get(w,'M');
  L = get(w,'L');
  C = get(w,'C');
  R = get(w,'R');
  Il = get(w,'Il');
  swingCostGainOpt = get(w,'swingCostGainOpt');
  %   Ip = get(w,'Ip');
  
  %%%%%%%%%%%%%%%%%% CONSTRAINTS %%%%%%%%%%%%%%%%%% 
  %% begin collision constraints
  % deal with collision first. 
  q1b = -alphaforSL; %stance angle is normally positive. at the end of a step just prior to collision it is negative.
  q2b = alphaforSL;
  u1b = u1precollision;%'b' before collision.
  u2b = u2precollision;
  
  MassMat = [ Il + C^2*M + L^2*M + L^2*Mp + 4*M*R^2 + 2*Mp*R^2 - 4*M*R^2*cos(q1b) - 2*Mp*R^2*cos(q1b) - 2*C*M*R - 2*L*M*R - 2*L*Mp*R + 2*C*M*R*cos(q1b) + 2*L*M*R*cos(q1b) + 2*L*Mp*R*cos(q1b), M*(C - L)*(R*cos(q2b) + L*cos(q1b - q2b) - R*cos(q1b - q2b)), 2*M*R*cos(q1b) - Mp*R - C*M*cos(q1b) - L*M*cos(q1b) - L*Mp*cos(q1b) - 2*M*R + Mp*R*cos(q1b), -sin(q1b)*(C*M + L*M + L*Mp - 2*M*R - Mp*R);
    M*(C - L)*(R*cos(q2b) + L*cos(q1b - q2b) - R*cos(q1b - q2b)),                                      M*C^2 - 2*M*C*L + M*L^2 + Il,                                                                             -M*cos(q2b)*(C - L),                         -M*sin(q2b)*(C - L);
    2*M*R*cos(q1b) - Mp*R - C*M*cos(q1b) - L*M*cos(q1b) - L*Mp*cos(q1b) - 2*M*R + Mp*R*cos(q1b),                                              -M*cos(q2b)*(C - L),                                                                                         2*M + Mp,                                            0;
    -sin(q1b)*(C*M + L*M + L*Mp - 2*M*R - Mp*R),                                              -M*sin(q2b)*(C - L),                                                                                                0,                                     2*M + Mp];
  
  % qdot includes x and y of the backfoot.
  qdot = [u1b;u2b;0;0];
  % note: x and y are both zero, because the back foot is on the ground.
  x =0;y=0;
  Jlambda_impulse = [0,0,1,0;
    0,0,0,1];
  angleCOM = atan2(-(L-R)*sin(q1b), R+(L-R)*cos(q1b)); %stolen from walk2uphill.
  
  delta_q = MassMat \ Jlambda_impulse'* [sin(angleCOM);cos(angleCOM)]*P;
  
  qdot = qdot+delta_q;
  
  uintr=qdot; %used below for energy calculations. 
  
  %%%%%%%%%%%% original collision code.
  Jlambda = [ - R - cos(q1b)*(L - R), R + cos(q2b)*(L - R), 1, 0;
    -sin(q1b)*(L - R),     sin(q2b)*(L - R), 0, 1];
  
  epsilon = 0;%%%% /system specific but usual for collisions.
  
  %%%% generic
  cdot = Jlambda*qdot;
  nc = length(cdot);
  
  Mlambda = MassMat \ Jlambda';
  Mlambda = Jlambda * Mlambda;
  impulse = -1* Mlambda \ ((eye(nc,nc)+eye(nc,nc)*epsilon)*cdot);
  qdot_new = qdot + MassMat\ Jlambda' * impulse;
  %%%% /generic
  
  %%specific
  unew = qdot_new;%since we do not want the x and y velocities to be returned
  % Boundary constraints (based on collision for the 'u's.
  cbnd = {
    initial(q1) == alphaforSL;
    initial(q2) == -alphaforSL;
    initial(u1) == unew(2); %these should look flipped because we swap legs.
    initial(u2) == unew(1); %these should look flipped because we swap legs.
    final(q1) == initial(q2);
    final(q2) == initial(q1);
    final(u1) == u1precollision;
    final(u2) == u2precollision;
  };
  %%/end collision constraints. initial ang vels should be specified by these. 

  %% begin collision costs
  q1b = -alphaforSL; q2b = alphaforSL;
  
  c1 = cos(q1b); c2 = cos(q2b); s1 = sin(q1b); s2 = sin(q2b); c12 = cos(q1b-q2b);
  KEstab = 1/2*M*((C^2-2*C*R+2*R^2+2*(C-R)*R*c1)*u1b^2) + 1/2*Il*u1b^2;
  KEswib = 1/2*M*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1b^2 + ...
  2*u1b*((C-L)*((L-R)*c12+R*c2)*u2b) + (C-L)^2*u2b^2) + 1/2*Il*u2b^2;
  KEpelvb = 1/2*Mp*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1b^2);
  
  eb = KEstab + KEswib + KEpelvb;

  %%% intermediate state energy.
  u1i = uintr(1); u2i = uintr(2); u3i = uintr(3); u4i = uintr(4); %u3 and u4 are xdot and ydot.
  % now we can compute the kinetic energy in the intermediate state
  KEstai = 1/2*M*((C^2-2*C*R+2*R^2+2*(C-R)*R*c1)*u1i^2 + u3i^2 + u4i^2 + ...
    2*u1i*((-R+(-C+R)*c1)*u3i + (-C+R)*s1*u4i)) + 1/2*Il*u1i^2;
  KEswii = 1/2*M*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1i^2 + (C-L)^2*u2i^2 + u3i^2 + u4i^2 + ...
    2*u1i*((C-L)*((L-R)*c12+R*c2)*u2i + (-R + (-L+R)*c1)*u3i + ...
    (-L+R)*s1*u4i) - 2*(C-L)*u2i*(c2*u3i+s2*u4i)) + 1/2*Il*u2i^2;
  KEpelvi = 1/2*Mp*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1i^2 + u3i^2 + u4i^2 + ...
    2*u1i*((-R + (-L+R)*c1)*u3i + (-L+R)*s1*u4i));
  ei = KEstai + KEswii + KEpelvi;

  % after impulse kinetic energy.   
  u1a = unew(2);u2a = unew(1);
  c1 = cos(q2b); c2 = cos(q1b); %c12 = cos(q2e-q1e);% maybe usually unnecessary. 
  KEstaa = 1/2*M*((C^2-2*C*R+2*R^2+2*(C-R)*R*c1)*u1a^2) + 1/2*Il*u1a^2;
  KEswia = 1/2*M*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1a^2 + ...
    2*u1a*((C-L)*((L-R)*c12+R*c2)*u2a) + (C-L)^2*u2a^2) + 1/2*Il*u2a^2;
  KEpelva = 1/2*Mp*((L^2-2*L*R+2*R^2+2*(L-R)*R*c1)*u1a^2);

  
  ea = KEstaa + KEswia + KEpelva;

  
  %% dynamics constraints
  sg = sin(gamma);
  cg = cos(gamma);
  c1 = cos(q1); c2 = cos(q2); c12 = cos(q1-q2);
  s1 = sin(q1); s2 = sin(q2); s12 = sin(q1-q2);
  
  %   MM = zeros(2,2); rhs = zeros(2,1);
  % trying to debug this error:
  % The following error occurred converting from tomSym to double:
  % Cannot convert symbolic tomSym object to double.
  % does not like:
  % 1) a re-run where MM is being changed.
  % 2) if MM is defined as a double, and tomsym objects are part of the
  % equations.
  clear MM rhs
  mata = Il - 2*C*M*R + 2*C*c1*M*R - 2*L*M*R + 2*c1*L*M*R - 2*L*Mp*R + ...
    2*c1*L*Mp*R + M*(C*C) + M*(L*L) + Mp*(L*L) + 4*M*(R*R) - ...
    4*c1*M*(R*R) + 2*Mp*(R*R) - 2*c1*Mp*(R*R);
  
  matb = C*c12*L*M - C*c12*M*R + C*c2*M*R + c12*L*M*R - c2*L*M*R - ...
    c12*M*(L*L);
  
  %   MM(2,1) = MM(1,2);
  
  matd = Il - 2*C*L*M + M*(C*C) + M*(L*L);
  
  MMinv = 1/(mata*matd-matb*matb)*[matd,-matb;-matb,mata];
  
  rhs(1,1) = -(Kp*(q1 - q2 - KpPhi0)) - cg*g*M*(-C + R)*s1 - cg*g*M*(-L + R)*s1 - ...
    cg*g*Mp*(-L + R)*s1 + (-(g*M*R) + c1*g*M*(-C + R))*sg +  ...
    (-(g*M*R) + c1*g*M*(-L + R))*sg +  ...
    (-(g*Mp*R) + c1*g*Mp*(-L + R))*sg +  ...
    s1*(C*M*R + L*M*R + L*Mp*R - 2*M*(R*R) - Mp*(R*R))*  ...
    (u1*u1) + ((C*M*R - L*M*R)*s2 +  ...
    s12*(-(C*L*M) + C*M*R - L*M*R + M*(L*L)))*(u2*u2);
  
  rhs(2,1) = -(Kp*(-1*(q1 - q2 - KpPhi0))) + s12*(C*L*M - C*M*R + L*M*R - M*(L*L))* ...
    (u1*u1) + g*(-C + L)*M*sin(gamma - q2);
  
  udot = MMinv * rhs;
  
  % Equality dynamics contraints
  ceq = collocate({
    dot([q1;q2]) == [u1;u2]
    dot([u1;u2]) == udot
    });

  %% step length constraint.
  phiHalf1=initial(q1);
  phiHalf2=final(q2);
  sl = (L-R)*sin(phiHalf1) + R*phiHalf1 + (L-R)*sin(phiHalf2) + R*phiHalf2;
  cConstrainSL = {
    sl == slDes;
  };
  
  % velocity constraint
  cConstrainedSpeed = {
    sl/tEnd == vDes;
    };
  %% Box constraints
  % may want to prevent knee hyperextensions.
  cbox = {
    -1<=initial(u1) <=-.1
    -1<=initial(u2) <=-.1
    0 <= P 
    0 <= Kp <=.5
    };
  
  %% objectives: 
  q10 = initial(q1);
  q20 = initial(q2);
  q1f = final(q1);
  q2f = final(q2);
  
  PE0 = 0.5*Kp*(q10-q20 - KpPhi0)*(q10 - q20 - KpPhi0);
  PEf = 0.5*Kp*(q1f-q2f - KpPhi0)*(q1f - q2f - KpPhi0);
  
  cost_ei_minus_eb = ei - eb;
  cost_collision = ei - ea;
  
%   Ix = P*sin(angleCOM); Iy = P*cos(angleCOM); %
  cost_pushoff = ei-eb;
  cost_swing = abs(Kp)/tEnd*swingCostGainOpt;
  cost_legwork = PE0 - PEf;
  objective = cost_pushoff + cost_swing + cost_legwork;
  
  %% Solve the problem
  options = struct;
  options.name = ['SNOPT walking model: collisions calculating impulse.'];
  OPTIONS.PriLev = 2;
  options.Prob.SOL.optPar(35) = 100000;
  options.Prob.SOL.optPar(30) = 200000;
  Prob = sym2prob(objective, {cbnd, ceq, cbox, cConstrainSL, cConstrainedSpeed}, x0, options);
  result = tomRun('snopt',Prob,1);
  solution = getSolution(result);
  
  solutions(iloop)=solution;
  f_k=result.f_k;
  Exit=[result.ExitFlag result.Inform];
  
  
  q1opt = subs(q1, solution);
  q2opt = subs(q2, solution);
  u1opt = subs(u1, solution);
  u2opt = subs(u2, solution);
  tOpt = subs(tEnd,solution);
  KpOut = subs(Kp,solution);
  KpPhi0Out = KpPhi0;
  POut = subs(P,solution);
  u1Out = subs(u1precollision,solution);
  u2Out = subs(u2precollision,solution);
  
  t_plot = [0.00:0.001:tOpt,tOpt]; % Time vector where we want our trajectory evaluated.
  q1_plot = atPoints(t_plot,subs(q1,solution));
  q2_plot = atPoints(t_plot,subs(q2,solution));
  u1_plot = atPoints(t_plot,subs(u1,solution));
  u2_plot = atPoints(t_plot,subs(u2,solution));
  plot(t_plot,[q1_plot,q2_plot,u1_plot,u2_plot]);
  
  results.cost_legwork = subs(cost_legwork,solution);
  results.cost_ei_minus_eb = subs(cost_ei_minus_eb,solution);
  results.cost_collision = subs(cost_collision,solution);
  results.cost_pushoff = subs(cost_pushoff,solution);
  results.cost_swing = subs(cost_swing,solution);
  results.costNet = subs(objective,solution);
  fprintf('TOMLAB net = %.4f,pushoff = %.4f swing = %.4f legwork = %.4f COL i-a %.4f PUSH i-b %.4f\n',results.costNet,results.cost_pushoff,results.cost_swing,results.cost_legwork,results.cost_collision,results.cost_ei_minus_eb);
  
end
KpOut = subs(Kp,solution);
KpPhi0Out = KpPhi0;
POut = subs(P,solution);
u1Out = u1_plot(1);
u2Out = u2_plot(1);
w = walk2uphill('normal');
w = set(w,'Kp',KpOut);
w = set(w,'gamma',gamma);
w = set(w,'P',POut);
w = set(w,'KpPhi0',KpPhi0Out);
w = set(w,'P',POut);
xstar = [q1_plot(1),q2_plot(1),u1Out,u2Out];
w = set(w,'xstar',xstar);
w = set(w,'heelstrikeDetectionDirection',1);
w = set(w,'desiredTOpt',tOpt);
% w = set(w,'desiredTOpt',tOpt);
[xf,tf,X,T]=onesteptime(w);
figure(10);hold on;
set(gca,'ColorOrderIndex',1);
plot(t_plot,[q1_plot,q2_plot,u1_plot,u2_plot],'linewidth',3);
figure(11);hold on;
hold on;
plot(t_plot,-q1_plot-q2_plot); % someimtes the collision equation

results.t_plot = t_plot;
results.q1_plot = q1_plot;
results.q2_plot = q2_plot;
results.u1_plot = u1_plot;
results.u2_plot = u2_plot;
results.KpPu1u2KpPhi0 = [KpOut,POut,u1Out,u2Out,KpPhi0Out];
argList = {'Kp','P','xstar3opt','xstar4opt','KpPhi0'};
[costVal,cStruct] = costKuo2001General(results.KpPu1u2KpPhi0,w,argList);
fprintf('ODE45  net = %.4f,pushoff = %.4f swing = %.4f legwork = %.4f COL i-a %.4f PUSH i-b %.4f\n',...
  costVal,cStruct.pushoffWork,cStruct.swingCost,cStruct.swingLegWork,...
cStruct.collision,cStruct.pushoffWork);
results.costVal = costVal;
results.costStruct = cStruct;
results.tOpt = tOpt;
results.q1 = q1_plot(1);
results.xstar = xstar;
results.gamma = gamma;
results.vDes = vDes;
results.SL = slDes;
results.w = w;
results.xf = xf;
results.X = X;
results.T = T;
results.tf = tf;
endTomlab = [q1_plot(end),q2_plot(end),u1_plot(end),u2_plot(end)];
endSim = X(end,:);
fprintf('TOMLAB error %f %f %f %f \n',endTomlab-endSim);
results.ENDSim = X(end,:);
results.ENDTL = endTomlab;
results.ENDSimMinusOptError = endTomlab - endSim;

fName = [fPrefix,'gamma',round(num2str(gamma*10)),'sl',round(num2str(slDes*10)),'vDes',round(num2str(vDes*10))];
save(fName,'-struct','results')
figure(10);
plot(T,X,'color',[0,0,0]);