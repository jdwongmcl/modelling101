function [error,costStruct,xF,tF,x,t,Energies,w] = costKuo2001General(optParms,w,parmList)
% function [error,xF,te,x,T,Energies] = costKuo2001General(optParms,w,parmList)
% cost is Kuo2001 plus work of the swing leg. 
% simulate using onesteptime:
% we iteratively set via optParms the parameters of the leg so that we can
% in general optimize for whatever parameters we like...
for iList =1:length(parmList)
  w=set(w,parmList{iList},optParms(iList));
end

x1 = get(w,'xstar1opt');
x2 = -1*x1;
x3 = get(w,'xstar3opt');
x4 = get(w,'xstar4opt');

w=set(w,'xstar',[x1,x2,x3,x4]);
  
[xF,tF,x,t,Energies]= onesteptime(w);

% energy cost from jdw20181201 analysis: toe + .06 swing + PE 
% E_g = Energies.after.PEg - Energies.before.PEg;

% work cost
E_pushoff = Energies.intermediate.KE - Energies.before.KE;

% force rate from Kuo2001.
E_swing = abs(get(w,'Kp'))/t(end)*get(w,'swingCostGainOpt'); %0.065 replicates kuo.

% force rate from wong 2020:
Kp = get(w,'Kp');
c = get(w,'swingCostGainOpt');
alpha = x(:,1)-x(:,2);
force = Kp*alpha;
tlinspace = linspace(0,t(end),500);
dt = tlinspace(2)-tlinspace(1);
forceInt = interp1(t,force,tlinspace)';
dforce = grad5(forceInt,dt);
ddforce = grad5(dforce,dt);
forceRateCostJ = trapz(tlinspace,sqrt(ddforce.^2))*c; %jer's force rate cost. 

E_swingLegWork = 1*Energies.before.PEs - Energies.after.PEs;

EError = E_pushoff + E_swing + E_swingLegWork;

costStruct.fraterateint = trapz(tlinspace,sqrt(ddforce.^2));
costStruct.fratemax = max(dforce);
costStruct.swingCost = E_swing;
costStruct.pushoffWork = E_pushoff;
costStruct.swingLegWork = E_swingLegWork;
costStruct.collision = Energies.intermediate.KE - Energies.after.KE;
costStruct.totalCostWong2020 = forceRateCostJ + E_pushoff + E_swingLegWork;
costStruct.forceRateCostJ = forceRateCostJ;
costStruct.totalCostKuo2001 = E_pushoff + E_swing + E_swingLegWork;

error = costStruct.totalCostWong2020/1e3;

% error = 1;
% fprintf('skipping cost to see if it is a problem\n');