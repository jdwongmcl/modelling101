%goRunKuo2001
% note: you will have to have the $dynamicwalking/src folder already in
% your path, i.e. addpath('D:\users\jdwong\dynamicwalking\src'). 
% note also: avoid doing addpath(genpath()) on the above dynamicwalking
% folder because it contains old object oriented programming files that do
% not play well with the pathing from genpath(). 
w = walk2uphill('normal');
vDes = .4;
slDes = 0.7;
gamma = 0
KpPhi0In = 0;
fPrefix = 'test';
%%
optimizeSnoptKuo2001PFSweepKpPhi0GivenVdesSL(w,vDes,slDes,gamma,KpPhi0In,fPrefix)
optimizeSnoptAWContinuousMechanicalGivenVdesSL(w,vDes,slDes,gamma,fPrefix);